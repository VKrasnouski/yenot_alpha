//
//  YNEventRealm.m
//  Yenot
//
//  Created by FanLee on 22/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import "YNEventRealm.h"


@implementation YNEventRealm

- (id)initWithMantleModel:(YNEvent *)yenotEvent{
    
    self = [super init];
    if(!self) return nil;
   
    _ID = yenotEvent.ID;
    _name = yenotEvent.name;
    _eventDescription = yenotEvent.descript;
    _timeZone = yenotEvent.timezone;
    _lat = yenotEvent.lat;
    _lon = yenotEvent.lon;
    _eventPlaceUID = yenotEvent.placeUID;
    _maxMembers = yenotEvent.maxMembers;
    _cancelled = yenotEvent.cancelled;
    _type = [NSNumber numberWithInt: yenotEvent.type];
    _version = yenotEvent.version;
    _locationName = yenotEvent.locationName;
    _locationAddress = yenotEvent.locationAddress;
    
    
    
    return self;
    
}

@end
