//
//  YNEventRealm.h
//  Yenot
//
//  Created by FanLee on 22/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <Realm/Realm.h>
#import "YNEvent.h"


@interface YNEventRealm : RLMObject

@property NSNumber<RLMInt> *ID;
@property NSNumber<RLMInt> *organizerID;
@property NSString *name;
@property NSString *eventDescription;
@property NSString *eventPlaceUID;
@property NSString *timeZone;
@property NSNumber<RLMInt> *lat;
@property NSNumber<RLMInt> *lon;
@property NSNumber<RLMInt> *maxMembers;
@property BOOL cancelled;
@property NSNumber<RLMInt> *type;
@property NSNumber<RLMInt> *version;
@property NSString *locationName;
@property NSString *locationAddress;

- (id)initWithMantleModel:(YNEvent *)yenotEvent;


@end
