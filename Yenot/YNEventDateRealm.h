//
//  YNEventDateRealm.h
//  Yenot
//
//  Created by FanLee on 22/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <Realm/Realm.h>
#import "YNEventDate.h"


@interface YNEventDateRealm : RLMObject

@property NSNumber<RLMInt> *ID;
@property NSNumber<RLMInt> *eventID;
@property NSNumber<RLMInt> *subEventID;
@property NSNumber<RLMDouble> *dateTimeStart;
@property NSNumber<RLMDouble> *dateTimeEnd;
@property NSNumber<RLMInt> *timeStart;
@property NSNumber<RLMInt> *timeEnd;
@property NSData *days;
@property BOOL cancelled;
@property NSNumber<RLMInt> *version;
@property NSString *period;
@property NSString *periodInterval;



- (id)initWithMantleModel:(YNEventDate *)yenotEventDate;

@end
