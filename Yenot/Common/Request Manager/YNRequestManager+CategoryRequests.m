//
//  YNRequestManager+CategoryRequests.m
//  Yenot
//
//  Created by FanLee on 20.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNRequestManager+CategoryRequests.h"

@implementation YNRequestManager (CategoryRequests)

+ (void)requestPOSTRegistrationStartCallWithPhoneNumber:(YNDTONumber *)phoneNumber
                                                success:(void (^)(YNPassword *password))success
                                                failure:(void (^)(NSError *error))failure
{
    NSString *url = @"registration/start/call";
    
    [YNRequestManager request:YNPOST
                        toURL:url
            withAuthorization:NO
                  inputObject:phoneNumber
            outputObjectClass:[YNPassword class]
                      success:success
                      failure:failure];
}

+ (void)requestPOSTRegistrationStartSMSWithPhoneNumber:(YNDTONumber *)phoneNumber
                                               success:(void (^)(YNPassword *password))success
                                               failure:(void (^)(NSError *error))failure
{
    NSString *url = @"registration/start/message";
    
    [YNRequestManager request:YNPOST
                        toURL:url
            withAuthorization:NO
                  inputObject:phoneNumber
            outputObjectClass:[YNPassword class]
                      success:success
                      failure:failure];
}

+ (void)requestPOSTRegistrationFinishSMSWithRegistrationUserData:(YNDTORegistrationUserData *)userData
                                                         success:(void (^)(YNUserID *userID))success
                                                         failure:(void (^)(NSError *error))failure
{
    NSString *url = @"registration/finish";
    
    [YNRequestManager request:YNPOST
                        toURL:url
            withAuthorization:NO
                  inputObject:userData
            outputObjectClass:[YNUserID class]
                      success:success
                      failure:failure];
}

+ (void)requestPOSTFilterPhoneNumbersForYenotUsers:(YNNumbers *)numbers
                                           success:(void (^)(YNNumbers *yenotNumbers))success
                                           failure:(void (^)(NSError *error))failure
{
    NSNumber *userID = [YNRequestManager userIDWithFailure:failure];
    
    if (!userID) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"resources/%ld/contacts/numbers/filter", (unsigned long)[userID unsignedIntegerValue]];
    
    [YNRequestManager request:YNPOST
                        toURL:url
            withAuthorization:YES
                  inputObject:numbers
            outputObjectClass:[YNNumbers class]
                      success:success
                      failure:failure];
}

+ (void)requestGETRelatedEventsWithSuccess:(void (^)(YNRelatedEvents *relatedEvents))success
                                   failure:(void (^)(NSError *error))failure
{
    NSNumber *userID = [YNRequestManager userIDWithFailure:failure];
    
    if (!userID) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"resources/%ld/events/all", (unsigned long)[userID unsignedIntegerValue]];
    
    [YNRequestManager request:YNGET
                        toURL:url
            withAuthorization:YES
                  inputObject:nil
            outputObjectClass:[YNRelatedEvents class]
                      success:success
                      failure:failure];
}

+ (void)requestGETNewParticipationsIDsWithSuccess:(void (^)(YNIDs *ids))success
                                          failure:(void (^)(NSError *error))failure
{
    NSNumber *userID = [YNRequestManager userIDWithFailure:failure];
    
    if (!userID) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"resources/%ld/sync/participations/new", (unsigned long)[userID unsignedIntegerValue]];
    
    [YNRequestManager request:YNGET
                        toURL:url
            withAuthorization:YES
                  inputObject:nil
            outputObjectClass:[YNIDs class]
                      success:success
                      failure:failure];
}

+ (void)requestGETEventsWithParticipationsIDs:(YNIDs *)ids
                                      success:(void (^)(YNEvents *events))success
                                      failure:(void (^)(NSError *error))failure
{
    NSNumber *userID = [YNRequestManager userIDWithFailure:failure];
    
    if (!userID) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"resources/%ld/events/by/participations/ids", (unsigned long)[userID unsignedIntegerValue]];
    
    [YNRequestManager request:YNPOST
                        toURL:url
            withAuthorization:YES
                  inputObject:ids
            outputObjectClass:[YNEvents class]
                      success:success
                      failure:failure];
}

+ (void)requestGETEventsWithIDs:(YNIDs *)ids
                        success:(void (^)(YNEvents *events))success
                        failure:(void (^)(NSError *error))failure
{
    NSNumber *userID = [YNRequestManager userIDWithFailure:failure];
    
    if (!userID) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"resources/%ld/events/by/dates/ids", (unsigned long)[userID unsignedIntegerValue]];
    
    [YNRequestManager request:YNPOST
                        toURL:url
            withAuthorization:YES
                  inputObject:ids
            outputObjectClass:[YNEvents class]
                      success:success
                      failure:failure];
}

+ (void)requestGETParticipationsWithIDs:(YNIDs *)ids
                                success:(void (^)(YNParticipations *participations))success
                                failure:(void (^)(NSError *error))failure
{
    NSNumber *userID = [YNRequestManager userIDWithFailure:failure];
    
    if (!userID) {
        return;
    }
    
    NSString *url = [NSString stringWithFormat:@"resources/%ld/participations/ids", (unsigned long)[userID unsignedIntegerValue]];
    
    [YNRequestManager request:YNGET
                        toURL:url
            withAuthorization:YES
                  inputObject:nil
            outputObjectClass:[YNParticipations class]
                      success:success
                      failure:failure];
}

@end
