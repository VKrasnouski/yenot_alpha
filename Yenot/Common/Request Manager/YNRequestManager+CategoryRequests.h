//
//  YNRequestManager+CategoryRequests.h
//  Yenot
//
//  Created by FanLee on 20.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNRequestManager.h"
#import "YNError.h"
#import "YNUserID.h"
#import "YNDTONumber.h"
#import "YNNumbers.h"
#import "YNPassword.h"
#import "YNDTORegistrationUserData.h"
#import "YNRelatedEvents.h"
#import "YNIDs.h"
#import "YNEvents.h"
#import "YNParticipations.h"

@interface YNRequestManager (CategoryRequests)

/*!
 @brief Регистрация пользователя посредством телефонного звонка
 */
+ (void)requestPOSTRegistrationStartCallWithPhoneNumber:(YNDTONumber *)phoneNumber
                                                success:(void (^)(YNPassword *password))success
                                                failure:(void (^)(NSError *error))failure;

/*!
 @brief Регистрация пользователя посредством отправка SMS-сообщения
 */
+ (void)requestPOSTRegistrationStartSMSWithPhoneNumber:(YNDTONumber *)phoneNumber
                                               success:(void (^)(YNPassword *password))success
                                               failure:(void (^)(NSError *error))failure;

/*!
 @brief Завершение регистрации пользователя посредством SMS-сообщения
 */
+ (void)requestPOSTRegistrationFinishSMSWithRegistrationUserData:(YNDTORegistrationUserData *)userData
                                                         success:(void (^)(YNUserID *ID))success
                                                         failure:(void (^)(NSError *error))failure;

/*!
 @brief Запрос предназначен для определения пользователей системы «YeNot» среди перечня записей телефонной книги, сервер анализирует набор телефонных номеров и определяет, какие номера зарегистрированы в системе
 */
+ (void)requestPOSTFilterPhoneNumbersForYenotUsers:(YNNumbers *)numbers
                                           success:(void (^)(YNNumbers *yenotNumbers))success
                                           failure:(void (^)(NSError *error))failure;

/*!
 @brief Запрос предназначен для получения списков связанных с пользователем событий
 */
+ (void)requestGETRelatedEventsWithSuccess:(void (^)(YNRelatedEvents *relatedEvents))success
                                   failure:(void (^)(NSError *error))failure;

+ (void)requestGETNewParticipationsIDsWithSuccess:(void (^)(YNIDs *ids))success
                                          failure:(void (^)(NSError *error))failure;

+ (void)requestGETEventsWithParticipationsIDs:(YNIDs *)ids
                                      success:(void (^)(YNEvents *events))success
                                      failure:(void (^)(NSError *error))failure;

+ (void)requestGETEventsWithIDs:(YNIDs *)ids
                        success:(void (^)(YNEvents *events))success
                        failure:(void (^)(NSError *error))failure;

+ (void)requestGETParticipationsWithIDs:(YNIDs *)ids
                                success:(void (^)(YNParticipations *participations))success
                                failure:(void (^)(NSError *error))failure;

@end
