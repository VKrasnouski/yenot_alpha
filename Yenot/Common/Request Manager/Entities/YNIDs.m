//
//  YNIDs.m
//  Yenot
//
//  Created by FanLee on 20.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNIDs.h"

@implementation YNIDs

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"ids" : @"ids",
             };
}

+ (NSValueTransformer *)idsJSONTransformer
{
    return nil;
}

@end
