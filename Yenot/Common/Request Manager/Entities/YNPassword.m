//
//  YNPassword.m
//  Yenot
//
//  Created by FanLeeon 15.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNPassword.h"

@implementation YNPassword

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"password" : @"password"
             };
}

@end
