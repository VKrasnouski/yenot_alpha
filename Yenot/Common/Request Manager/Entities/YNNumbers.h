//
//  YNNumbers.h
//  Yenot
//
//  Created by FanLee on 14.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNNumbers : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSArray *numbers;

@end
