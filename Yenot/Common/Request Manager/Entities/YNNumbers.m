//
//  YNNumbers.m
//  Yenot
//
//  Created by FanLee on 14.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNNumbers.h"

@implementation YNNumbers

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"numbers" : @"numbers"
             };
}

+ (NSValueTransformer *)numbersJSONTransformer
{
    return nil;
}

@end
