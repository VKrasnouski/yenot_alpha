//
//  YNUserID.m
//  Yenot
//
//  Created by FanLee on 16.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNUserID.h"

@implementation YNUserID

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"ID" : @"userId",
             @"deviceID" : @"deviceId"
             };
}

@end
