//
//  YNUserID.h
//  Yenot
//
//  Created by FanLee on 16.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNUserID : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *ID;

@property (nonatomic, strong) NSNumber *deviceID;

@end
