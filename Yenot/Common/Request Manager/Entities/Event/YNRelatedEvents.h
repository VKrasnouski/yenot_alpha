//
//  YNRelatedEvents.h
//  Yenot
//
//  Created by FanLee on 03.04.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YNParticipantEvent.h"

@interface YNRelatedEvents : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSArray <YNParticipantEvent *> *organizerEvents;

@property (nonatomic, strong) NSArray <YNParticipantEvent *> *participantEvents;

@end
