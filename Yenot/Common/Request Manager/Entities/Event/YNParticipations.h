//
//  YNParticipations.h
//  Yenot
//
//  Created by FanLee on 22.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "YNParticipation.h"

@interface YNParticipations : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSArray <YNParticipation *> *participations;

@end
