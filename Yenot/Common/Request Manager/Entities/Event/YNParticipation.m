//
//  YNParticipation.m
//  Yenot
//
//  Created by FanLee on 22.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNParticipation.h"

@implementation YNParticipation

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"ID" : @"id",
             @"duration" : @"duration",
             @"confirmed" : @"confirmed",
             @"canInvite" : @"canInvite",
             @"date" : @"date",
             @"version" : @"version"
             };
}

+ (NSValueTransformer *)canInviteJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

@end
