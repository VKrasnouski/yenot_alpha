//
//  YNRelatedEvents.m
//  Yenot
//
//  Created by FanLee on 03.04.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNRelatedEvents.h"

@implementation YNRelatedEvents

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"organizerEvents" : @"organizerEvents",
             @"participantEvents" : @"participantEvents"
             };
}

+ (NSValueTransformer *)organizerEventsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:YNParticipantEvent.class];
}

+ (NSValueTransformer *)participantEventsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:YNParticipantEvent.class];
}

@end
