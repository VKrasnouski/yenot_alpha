//
//  YNEvents.h
//  Yenot
//
//  Created by FanLee on 22.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNEvents : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSArray <YNParticipantEvent *> * events;

@end
