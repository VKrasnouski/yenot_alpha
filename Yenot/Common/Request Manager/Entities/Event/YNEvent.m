//
//  YNEvent.m
//  Yenot
//
//  Created by FanLee on 03.04.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNEvent.h"

@implementation YNEvent

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"ID" : @"id",
             @"type" : @"type",
             @"version" : @"version",
             @"cancelled" : @"cancelled",
             @"name" : @"name",
             @"descript" : @"description",
             @"placeUID" :  @"placeUID",
             @"timezone" : @"timezone",
             @"maxMembers" : @"maxMembers",
             @"locationName" : @"locationName",
             @"locationAddress" : @"locationAddress",
             @"lat" : @"lat",
             @"lon" : @"lon"
             };
}

+ (NSValueTransformer *)cancelledJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

+ (NSValueTransformer *)typeJSONTransformer
{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@0 : @(YNEventTypeDefault),
                                                                           @1 : @(YNEventTypeComplicated),
                                                                           @2 : @(YNEventTypeSpecific)
                                                                           }];
}

@end
