//
//  YNParticipations.m
//  Yenot
//
//  Created by FanLee on 22.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNParticipations.h"

@implementation YNParticipations

- (instancetype)init
{
    if (self = [super init]) {
        _participations = [[NSArray alloc] init];
    }
    
    return self;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"participations" : @"participations"
             };
}

+ (NSValueTransformer *)participationsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:YNParticipation.class];
}

@end
