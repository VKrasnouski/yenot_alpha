//
//  YNParticipation.h
//  Yenot
//
//  Created by FanLee on 22.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface YNParticipation : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *ID;

@property (nonatomic, strong) NSNumber *duration;

@property (nonatomic, strong) NSNumber *confirmed;

@property (nonatomic, assign) BOOL canInvite;

@property (nonatomic, strong) NSNumber *date;

@property (nonatomic, strong) NSNumber *version;

@end
