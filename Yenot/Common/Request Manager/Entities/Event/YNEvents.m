//
//  YNEvents.m
//  Yenot
//
//  Created by FanLee on 22.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNEvents.h"

@implementation YNEvents

- (instancetype)init
{
    if (self = [super init]) {
        _events = [[NSArray alloc] init];
    }
    
    return self;
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"events" : @"events"
             };
}

+ (NSValueTransformer *)eventsJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:YNParticipantEvent.class];
}

@end
