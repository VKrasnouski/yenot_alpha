//
//  YNEvent.h
//  Yenot
//
//  Created by FanLee on 03.04.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, YNModelEventType)
{
    YNEventTypeDefault,
    YNEventTypeComplicated,
    YNEventTypeSpecific
};

@interface YNEvent : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *ID;

@property (nonatomic, assign) YNModelEventType type;

@property (nonatomic, strong) NSNumber *version;

@property (nonatomic, assign) BOOL cancelled;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *descript;

@property (nonatomic, copy) NSString *placeUID;

@property (nonatomic, copy) NSString *timezone;

@property (nonatomic, strong) NSNumber *maxMembers;

@property (nonatomic, copy) NSString *locationName;

@property (nonatomic, copy) NSString *locationAddress;

@property (nonatomic, strong) NSNumber *lat;

@property (nonatomic, strong) NSNumber *lon;

@end
