//
//  YNParticipantEvent.h
//  Yenot
//
//  Created by FanLee on 03.04.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNParticipantEvent : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) YNEvent *event;

@property (nonatomic, strong) NSArray <YNEventDate *> *eventDates;

@property (nonatomic, strong) NSArray <NSNumber *> *organizerNumbers;

@property (nonatomic, strong) NSArray *subevents;

@end
