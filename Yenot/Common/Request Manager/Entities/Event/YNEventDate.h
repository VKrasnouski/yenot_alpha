//
//  YNEventDate.h
//  Yenot
//
//  Created by FanLee on 03.04.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNEventDate : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *ID;

@property (nonatomic, strong) NSNumber *version;

@property (nonatomic, copy) NSString *period;

@property (nonatomic, copy) NSString *periodInterval;

@property (nonatomic, assign) BOOL cancelled;

@property (nonatomic, strong) NSNumber *eventId;

@property (nonatomic, strong) NSNumber *subeventId;

//@property (nonatomic, strong) NSMutableArray <NSNumber *> *days;

@property (nonatomic, strong) NSNumber *datetimeEnd;

@property (nonatomic, strong) NSNumber *datetimeStart;

@property (nonatomic, strong) NSNumber *timeEnd;

@property (nonatomic, strong) NSNumber *timeStart;

@end
