//
//  YNParticipantEvent.m
//  Yenot
//
//  Created by FanLee on 03.04.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNParticipantEvent.h"

@implementation YNParticipantEvent

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"event" : @"event",
             @"eventDates" : @"eventDates",
             @"organizerNumbers" : @"organizerNumbers"
             };
}

+ (NSValueTransformer *)eventJSONTransformer
{
    return [MTLJSONAdapter dictionaryTransformerWithModelClass:YNEvent.class];
}

+ (NSValueTransformer *)eventDatesJSONTransformer
{
    return [MTLJSONAdapter arrayTransformerWithModelClass:YNEventDate.class];
}

+ (NSValueTransformer *)organizerNumbersJSONTransformer
{
    return nil;
}

@end
