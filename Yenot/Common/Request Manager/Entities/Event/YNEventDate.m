//
//  YNEventDate.m
//  Yenot
//
//  Created by FanLee on 03.04.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNEventDate.h"

@implementation YNEventDate

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"ID" : @"id",
             @"version" : @"version",
             @"period" : @"period",
             @"periodInterval" : @"periodInterval",
             @"cancelled" : @"cancelled",
             @"eventId" : @"eventId",
             @"subeventId" : @"subeventId",
             @"days" : @"days",
             @"datetimeEnd" : @"datetimeEnd",
             @"datetimeStart" : @"datetimeStart",
             @"timeEnd" : @"timeEnd",
             @"timeStart" : @"timeStart"
             };
}

+ (NSValueTransformer *)cancelledJSONTransformer
{
    return [NSValueTransformer valueTransformerForName:MTLBooleanValueTransformerName];
}

+ (NSValueTransformer *)daysJSONTransformer
{
    return nil;
}

@end
