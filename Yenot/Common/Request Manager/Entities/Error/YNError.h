//
//  YNError.h
//  Yenot
//
//  Created by FanLee on 26.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNError : MTLModel <MTLJSONSerializing>

@property (nonatomic, copy) NSString *message;

@property (nonatomic, strong) NSNumber *returnCode;

@end
