//
//  YNError.m
//  Yenot
//
//  Created by FanLee on 26.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNError.h"

@implementation YNError

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"message" : @"message",
             @"returnCode" : @"returnCode"
             };
}

@end
