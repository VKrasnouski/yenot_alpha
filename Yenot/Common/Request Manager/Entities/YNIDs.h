//
//  YNIDs.h
//  Yenot
//
//  Created by FanLee on 20.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNIDs : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSArray <NSNumber *> *ids;

@end
