//
//  YNDTONumber.h
//  Yenot
//
//  Created by FanLee on 15.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNDTONumber : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *number;

@end
