//
//  YNDTORegistrationUserData.h
//  Yenot
//
//  Created by FanLee on 15.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNDTORegistrationUserData : MTLModel <MTLJSONSerializing>

/*! @brief Имя пользователя */
@property (nonatomic, copy) NSString *nickname;

/*! @brief Полученный от сервера пароль */
@property (nonatomic, copy) NSString *password;

/*! @brief Регистрационный код, полученный посредством SMS указывается только в случае инициирования отправки SMS */
@property (nonatomic, strong) NSNumber *code;

/*! @brief Телефонный номер */
@property (nonatomic, strong) NSNumber *number;

/*! @brief Международный идентификатор устройства (imei) */
@property (nonatomic, copy) NSString *imei;

/*! @brief Идентификатор устройства для получения push-уведомлений */
@property (nonatomic, copy) NSString *notificationsApiId;

/*! @brief Телефонные номера записной книжки */
@property (nonatomic, strong) NSArray *numbers;

@end
