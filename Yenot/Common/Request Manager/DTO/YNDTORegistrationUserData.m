//
//  YNDTORegistrationUserData.m
//  Yenot
//
//  Created by FanLee on 15.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNDTORegistrationUserData.h"

@implementation YNDTORegistrationUserData

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"nickname" : @"nickname",
             @"password" : @"password",
             @"code" : @"code",
             @"number" : @"number",
             @"imei" : @"imei",
             @"notificationsApiId" : @"notificationsApiId",
             @"numbers" : @"numbers"
             };
}

+ (NSValueTransformer *)numbersJSONTransformer
{
    return nil;
}

@end
