//
//  YNDTONumber.m
//  Yenot
//
//  Created by FanLee on 15.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNDTONumber.h"

@implementation YNDTONumber

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"number" : @"number"
             };
}

@end
