//
//  YNRequestManager.m
//  Yenot
//
//  Created by FanLee on 01.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNRequestManager.h"
#import "YNNetworking.h"

@implementation YNRequestManager

    + (void)request:(YNMethod)method
              toURL:(NSString *)url
  withAuthorization:(BOOL)auth
        inputObject:(id <MTLJSONSerializing>)inObj
  outputObjectClass:(Class)oClass
            success:(void (^)(id))success
            failure:(void (^)(NSError *error))failure
{
    NSDictionary *params = nil;
    
    if (inObj)
    {
        @try {
            
            NSError *error;
            params = [MTLJSONAdapter JSONDictionaryFromModel:inObj error:&error];
            
            if (error)
            {
                NSError *convertError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_APPLICATION code:0 userInfo:error.userInfo];
                
                if (failure) {
                    failure(convertError);
                } else {
                    NSLog(@"[YNRequestManager] failure block is empty. Execution continues...");
                }
            }
            
        }
        @catch (NSException *exception) {
            
            NSError *excError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_APPLICATION code:0 userInfo:nil];
            
            if (failure) {
                failure(excError);
            } else {
                NSLog(@"[YNRequestManager] failure block is empty. Execution continues...");
            }
            
        }
        
        if (NETWORK_DEBUG_CONFIGURATION)
        {
            // Блок логирования данных протокола передачи данных на сервер
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:nil];
            
            if (jsonData)
            {
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                NSLog(@"[NETWORKING_DEBUG]\n%@", jsonString);
            }
            // Конец блока
        }
    }
    
    void (^successWrapper)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse) = ^void(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse) {
        
        if (NETWORK_DEBUG_CONFIGURATION)
        {
            // Блок логирования данных протокола передачи данных с сервера
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:responseObject options:NSJSONWritingPrettyPrinted error:nil];
            
            if (jsonData)
            {
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                NSLog(@"[NETWORKING_DEBUG]\n%@", jsonString);
            }
            // Конец блока
        }
        
        NSInteger code = httpResponse.statusCode;
        
        if (code >= 400 && code < 500)
        {
            // Ошибка клиента
            NSError *error;
            YNError *yenotError = [MTLJSONAdapter modelOfClass:[YNError class] fromJSONDictionary:responseObject error:&error];
            
            if (error)
            {
                NSError *convertError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_APPLICATION code:0 userInfo:error.userInfo];
                
                if (failure) {
                    failure(convertError);
                } else {
                    NSLog(@"[YNRequestManager] failure block is empty. Execution continues...");
                }
            }
            
            NSError *networkingError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_SERVER
                                                           code:[yenotError.returnCode integerValue]
                                                       userInfo:responseObject];

            if (failure) {
                failure(networkingError);
            } else {
                NSLog(@"[YNRequestManager] failure block is empty. Execution continues...");
            }
        }
        else
        {
            // Успешное завершение операции
            NSError *error;
            id outObj;
            
            @try {
                outObj = [MTLJSONAdapter modelOfClass:oClass fromJSONDictionary:responseObject error:&error];
            } @catch (NSException *exception)
            {
                NSError *excError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_APPLICATION code:0 userInfo:nil];
               
                if (failure) {
                    NSLog(@"[YNRequestManager] mantle parsing error. Check out your \"%@\" model class", NSStringFromClass(oClass));
                    failure(excError);
                } else {
                    NSLog(@"[YNRequestManager] failure block is empty. Stop execution!");
                }
                
                return;
            }
            
            if (error)
            {
                NSError *convertError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_APPLICATION code:0 userInfo:error.userInfo];
                
                if (failure) {
                    failure(convertError);
                } else {
                    NSLog(@"[YNRequestManager] failure block is empty. Execution continues...");
                }
            }
            
            if (success) {
                success(outObj);
            } else {
                NSLog(@"[YNRequestManager] success block is empty. Execution continues...");
            }
        }
    };
    
    void (^failureWrapper)(NSError *error, NSHTTPURLResponse *httpResponse) = ^void(NSError *error, NSHTTPURLResponse * _Nullable httpResponse) {
        
        if (error)
        {
            if (failure) {
                failure(error);
            } else {
                NSLog(@"[YNRequestManager] failure block is empty. Execution continues...");
            }
        }
        
    };
    
    if ([url isEmpty])
    {
        NSLog(@"[YNRequestManager] url with method name is empty.");
        NSError *error = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_VALIDATION code:0 userInfo:nil];
        failure(error);
        return;
    }
    
    switch (method) {
        case YNGET:
        {
            [YNNetworking requestGETWithUrl:url
                                 parameters:params
                              authorization:auth
                                    success:successWrapper
                                    failure:failureWrapper];
            break;
        }
        case YNPOST:
        {
            [YNNetworking requestPOSTWithUrl:url
                                  parameters:params
                               authorization:auth
                                     success:successWrapper
                                     failure:failureWrapper];
            break;
        }
        case YNPUT:
        {
            [YNNetworking requestPUTWithUrl:url
                                 parameters:params
                              authorization:auth
                                    success:successWrapper
                                    failure:failureWrapper];
            break;
        }
        case YNDELETE:
        {
            [YNNetworking requestDELETEWithUrl:url
                                    parameters:params
                                 authorization:auth
                                       success:successWrapper
                                       failure:failureWrapper];
            break;
        }
        default:
            break;
    }
}

+ (NSNumber *)userIDWithFailure:(void (^)(NSError *error))failure
{
    YNUserManager *userManager = [YNUserManager sharedInstance];
    NSNumber *userID = userManager.user.ID;
    
    if (!userManager || !userID)
    {
        NSError *userError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_APPLICATION
                                                 code:0
                                             userInfo:nil];
        if (failure) {
            failure(userError);
        } else {
            NSLog(@"[YNRequestManager] failure block is empty. Execution continues...");
        }
        
        return nil;
    }
    
    return userID;
}

@end
