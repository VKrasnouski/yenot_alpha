//
//  YNRequestManager.h
//  Yenot
//
//  Created by FanLee on 01.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    YNGET    = 1,
    YNPOST   = 2,
    YNPUT    = 3,
    YNDELETE = 4
} YNMethod;

@interface YNRequestManager : NSObject

    + (void)request:(YNMethod)method
              toURL:(NSString*)url
  withAuthorization:(BOOL)auth
        inputObject:(id<MTLJSONSerializing>)inObj
  outputObjectClass:(Class)oClass
            success:(void (^)(id))success
            failure:(void (^)(NSError *error))failure;

+ (NSNumber*)userIDWithFailure:(void (^)(NSError *error))failure;

@end
