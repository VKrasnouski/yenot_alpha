//
//  YNModelUser.h
//  Yenot
//
//  Created by FanLee on 24.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNModelUser : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *ID;

@property (nonatomic, copy) NSString *nickname;

@property (nonatomic, copy) NSString *password;

@property (nonatomic, strong) NSNumber *number;

@property (nonatomic, strong) UIImage *photo;

@end
