//
//  YNModelUser.m
//  Yenot
//
//  Created by FanLee on 24.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNModelUser.h"

@implementation YNModelUser

+ (NSDictionary *)JSONKeyPathsByPropertyKey
{
    return @{@"ID" : @"ID",
             @"nickname" : @"nickname",
             @"number" : @"number",
             @"password" : @"password"
             };
}

@end
