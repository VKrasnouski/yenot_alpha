//
//  YNUserManager.m
//  Yenot
//
//  Created by FanLee on 24.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNUserManager.h"

static NSString * const YENOT_USER_FILE_NAME = @"user_data.user";

static YNUserManager *userManager;

@implementation YNUserManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        userManager = [[YNUserManager alloc] init];
        userManager.user = nil;
    });

    return userManager;
}

- (void)saveUser
{
    NSData *jsonData = [NSKeyedArchiver archivedDataWithRootObject:_user];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths firstObject] stringByAppendingPathComponent:YENOT_USER_FILE_NAME];
    [[NSFileManager defaultManager] createFileAtPath:path
                                            contents:jsonData
                                          attributes:nil];
}

- (void)loadUser
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths firstObject] stringByAppendingPathComponent:YENOT_USER_FILE_NAME];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path])
    {
        NSData *userData = [[NSData alloc] initWithContentsOfFile:path];
        _user = nil;
        _user = (YNModelUser*)[NSKeyedUnarchiver unarchiveObjectWithData:userData];
    } else {
        NSLog(@"YNUserManager: user data file does not exist.");
    }
}

- (nullable NSString*)authorizationToken
{
    if (!_user || !_user.ID || !_user.password || [_user.password isEmpty]) {
        NSLog(@"YNUserManager: user data is empty.");
        return nil;
    }
    
    NSUInteger userIDint = [_user.ID unsignedIntegerValue];
    NSString *authorizationPair = [NSString stringWithFormat:@"%lu:%@", (long)userIDint, _user.password];
    NSString *encryptedAuthorizationPair = [authorizationPair toBase64String];
    NSString *authorizationToken = [NSString stringWithFormat:@"Basic %@", encryptedAuthorizationPair];
    
    return authorizationToken;
}

- (void)dealloc
{
    
}

@end
