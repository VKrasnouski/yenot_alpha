//
//  YNUserManager.h
//  Yenot
//
//  Created by FanLee on 24.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YNModelUser.h"

@interface YNUserManager : NSObject

@property (nonatomic, strong) YNModelUser *user;

+ (instancetype)sharedInstance;

- (void)saveUser;

- (void)loadUser;

- (NSString*)authorizationToken;

@end
