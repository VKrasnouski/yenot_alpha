//
//  UIAlertController+YNCategory.h
//  Yenot
//
//  Created by FanLee on 20.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIAlertController (YNCategory)

+ (void)showSimpleAlertWithTitle:(NSString *)alertTitle message:(NSString *)alertMessage onViewController:(UIViewController *)parentViewController;

@end
