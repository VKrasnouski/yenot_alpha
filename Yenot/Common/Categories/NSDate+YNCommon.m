//
//  NSDate+RPCommon.m
//  RPCustomer
//
//  Created by Alexandr Kurilovich on 18.04.16.
//  Copyright © 2016 Profigroup. All rights reserved.
//

#import "NSDate+YNCommon.h"

static NSString * const DATETIME_SERVER_FORMAT = @"yyyy-MM-dd'T'HH:mm:ss.SSSZ";

@implementation NSDate (YNCommon)

+ (NSDateFormatter *)serverDateFormatter
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
    dateFormatter.timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    dateFormatter.dateFormat = DATETIME_SERVER_FORMAT;
    
    return dateFormatter;
}

+ (NSDate *)dateFromServerString:(NSString *)dateString
{
    if (!dateString) {
        return nil;
    }
    
    NSDateFormatter *serverDateFormatter = [NSDate serverDateFormatter];
    NSDate *date = [serverDateFormatter dateFromString:dateString];
    
    return date;
}

- (NSString *)getDateStringByFormat:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
    formatter.dateFormat = format;
    
    return [formatter stringFromDate:self];
}

- (NSString *)transformDateToString
{
    NSString *onlyDateString;
    
    NSDateComponents *otherDay = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:self];
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSCalendarUnitEra | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:[[NSLocale preferredLanguages] objectAtIndex:0]];
    formatter.dateFormat = @"dd MMM";
    
    onlyDateString = [formatter stringFromDate:self];
    
    if ([today day] == [otherDay day] &&
        [today month] == [otherDay month] &&
        [today year] == [otherDay year] &&
        [today era] == [otherDay era])
    {
        onlyDateString = NSLocalizedString(@"common.today", nil);
    }
    
    if ([today day] - 1 == [otherDay day] &&
        [today month] == [otherDay month] &&
        [today year] == [otherDay year] &&
        [today era] == [otherDay era])
    {
        onlyDateString = NSLocalizedString(@"common.yesterday", nil);
    }
    
    formatter.dateFormat = @"HH:mm";
    NSString *timeString = [formatter stringFromDate:self];
    
    return [NSString stringWithFormat:@"%@, %@", onlyDateString, timeString];
}


- (NSString *)convertStringTimeIntervalToFormattedDateString {
    NSString *result;
    
    return result;
}

- (NSString *)convertStringTimeIntervalToRemainDaysCountString {
    
    NSDate *fromDate;
    NSDate *toDate;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                 interval:NULL forDate:[NSDate date]];
    [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                 interval:NULL forDate:self];
    
    NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                               fromDate:fromDate toDate:toDate options:0];
    
    return [NSString stringWithFormat:@"%ld", (long)[difference day]];
    
}

@end
