//
//  NSString+Converting.h
//  Yenot
//
//  Created by FanLee on 12.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Converting)

// Конвертирует строку телефонного номера в число
- (nullable NSNumber*)convertToPhoneNumber;

@end
