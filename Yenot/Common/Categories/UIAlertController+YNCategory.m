//
//  UIAlertController+YNCategory.m
//  Yenot
//
//  Created by FanLee on 20.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "UIAlertController+YNCategory.h"

@implementation UIAlertController (YNCategory)

+ (void)showSimpleAlertWithTitle:(NSString *)alertTitle message:(NSString *)alertMessage onViewController:(UIViewController *)parentViewController
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:alertTitle
                                                                             message:alertMessage
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    
    [alertController addAction:okAction];
    
    [parentViewController presentViewController:alertController animated:YES completion:nil];
}

@end
