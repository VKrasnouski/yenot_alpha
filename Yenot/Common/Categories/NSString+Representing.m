//
//  NSString+Representing.m
//  Yenot
//
//  Created by FanLee on 19.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "NSString+Representing.h"

@implementation NSString (Representing)

- (NSString*)stringByReplacingCharactersInString:(NSString*)characters withString:(NSString*)string
{
    NSCharacterSet *symbols = [NSCharacterSet characterSetWithCharactersInString:characters];
    
    return [[self componentsSeparatedByCharactersInSet:symbols] componentsJoinedByString:string];
}

- (NSString*)trimmingWhitespaceAndNewlineCharacters
{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
