//
//  NSString+Validation.h
//  Yenot
//
//  Created by FanLee on 03.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Validation)

/*! @brief Проверяет строку на наличие значимых символов. Не учитывает знаки переноса и пробелы. */
- (BOOL)isEmpty;

@end
