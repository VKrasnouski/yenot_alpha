//
//  NSString+Converting.m
//  Yenot
//
//  Created by FanLee on 12.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "NSString+Converting.h"

@implementation NSString (Converting)

- (nullable NSNumber*)convertToPhoneNumber
{
    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[^0-9]"
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    if (error) {
        return nil;
    }
    
    NSString *resultString = [regex stringByReplacingMatchesInString:self options:0 range:NSMakeRange(0, self.length) withTemplate:@""];
    
    if ([resultString isEmpty]) {
        return nil;
    }
    
    return [[NSNumberFormatter new] numberFromString:resultString];
}

@end
