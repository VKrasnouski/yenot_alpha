//
//  NSString+Validation.m
//  Yenot
//
//  Created by FanLee on 03.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "NSString+Validation.h"

@implementation NSString (Validation)

- (BOOL)isEmpty
{
    if ([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:[NSString string]]) {
        return YES;
    }
    
    return NO;
}

@end
