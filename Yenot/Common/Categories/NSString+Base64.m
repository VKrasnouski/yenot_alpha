//
//  NSString+Base64.m
//  Yenot
//
//  Created by FanLee on 01.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "NSString+Base64.h"

@implementation NSString (Base64)

- (NSString*)toBase64String
{
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64String = [data base64EncodedStringWithOptions:kNilOptions];
    
    return base64String;
}

- (NSString*)fromBase64String
{
    NSData *base64Data = [[NSData alloc] initWithBase64EncodedString:self options:kNilOptions];
    
    NSString* string = [[NSString alloc] initWithData:base64Data encoding:NSUTF8StringEncoding];
    
    return string;
}

@end
