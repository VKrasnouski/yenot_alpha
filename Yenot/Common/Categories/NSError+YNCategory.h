//
//  NSError+YNCategory.h
//  Yenot
//
//  Created by FanLee on 20.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

/*! @brief Домен ошибок сети */
static NSString * const YENOT_ERROR_DOMAIN_NETWORKING = @"YENOT_ERROR_DOMAIN_NETWORKING";
/*! @brief Домен ошибок сервера */
static NSString * const YENOT_ERROR_DOMAIN_SERVER = @"YENOT_ERROR_DOMAIN_SERVER";
/*! @brief Домен ошибок валидации полей данных */
static NSString * const YENOT_ERROR_DOMAIN_VALIDATION = @"YENOT_ERROR_DOMAIN_VALIDATION";
/*! @brief Домен ошибок приложения */
static NSString * const YENOT_ERROR_DOMAIN_APPLICATION = @"YENOT_ERROR_DOMAIN_APPLICATION";

@interface NSError (YNCategory)

@end
