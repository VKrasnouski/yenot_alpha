//
//  NSString+Base64.h
//  Yenot
//
//  Created by FanLee on 01.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Base64)

- (NSString*)toBase64String;

- (NSString*)fromBase64String;

@end
