//
//  NSString+Representing.h
//  Yenot
//
//  Created by FanLee on 19.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Representing)

/*! @brief Заменяет все символы, входящие в строку characters, в целевой строке на строку string. */
- (NSString*)stringByReplacingCharactersInString:(NSString*)characters withString:(NSString*)string;

/*! @brief Удаляет пробелы и знаки переноса в начале и в конце строки. */
- (NSString*)trimmingWhitespaceAndNewlineCharacters;

@end
