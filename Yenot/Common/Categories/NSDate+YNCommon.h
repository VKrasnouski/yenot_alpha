//
//  NSDate+RPCommon.h
//  RPCustomer
//
//  Created by Alexandr Kurilovich on 18.04.16.
//  Copyright © 2016 Profigroup. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (YNCommon)

/*! @brief Возвращает экземпляр NSDateFormatter учитывающий текущую пользовательскую локаль и формат даты сервера. */
+ (NSDateFormatter *)serverDateFormatter;

/*! @brief На основе строки полученной с сервера, метод возвращает экземпляр даты NSDate, учитывая текущую пользовательскую локаль и формат даты сервера. */
+ (NSDate *)dateFromServerString:(NSString *)dateString;

/*! @brief Возвращает дату в отформатированном виде (dd MMM, hh:mm) */
- (NSString *)transformDateToString;

/*! @brief Возвращает дату в отформатированном виде (dd MMM, hh:mm) из интервала в вормате ISO 8061 */
- (NSString *)convertStringTimeIntervalToFormattedDateString;

/*! @brief Возвращает оставшееся количество дней, полученное из интервала в вормате ISO 8061 */
- (NSString *)convertStringTimeIntervalToRemainDaysCountString;

/*! @brief Вывод даты в указанном формате */
- (NSString *)getDateStringByFormat:(NSString *)format;

@end
