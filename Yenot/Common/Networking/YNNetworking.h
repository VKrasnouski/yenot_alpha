//
//  YNNetworking.h
//  Yenot
//
//  Created by FanLee on 31.01.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifdef DEBUG
    #define NETWORK_DEBUG_CONFIGURATION     YES     // Измените данное значние на NO, чтобы не логировать данные протокола передачи в режиме отладки
#else
    #define NETWORK_DEBUG_CONFIGURATION     NO
#endif

@interface YNNetworking : NSObject

+ (void)requestGETWithUrl:(NSString*)urlString
               parameters:(NSDictionary*)params
            authorization:(BOOL)auth
                  success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
                  failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure;

+ (void)requestPOSTWithUrl:(NSString*)urlString
                parameters:(NSDictionary*)params
             authorization:(BOOL)auth
                   success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
                   failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure;

+ (void)requestPUTWithUrl:(NSString*)urlString
               parameters:(NSDictionary*)params
            authorization:(BOOL)auth
                  success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
                  failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure;

+ (void)requestDELETEWithUrl:(NSString*)urlString
                  parameters:(NSDictionary*)params
               authorization:(BOOL)auth
                     success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
                     failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure;

@end
