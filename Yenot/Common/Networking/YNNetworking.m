//
//  YNNetworking.m
//  Yenot
//
//  Created by FanLee on 31.01.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNNetworking.h"

#define DEFAULT_TIMEOUT_INTERVAL 40.0

static NSString * const SERVER_BASE_URL = @"http://185.18.52.203:8080/yenot2/";

@implementation YNNetworking

+ (nullable AFHTTPSessionManager*)defaultSessionManagerWithAuthorization:(BOOL)auth
{
    NSURL *serverBaseUrl = [NSURL URLWithString:SERVER_BASE_URL];
    
    if (!serverBaseUrl) {
        NSLog(@"YNNetworking: incorrect server base URL.");
        return nil;
    }
    
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    if (!sessionConfiguration) {
        NSLog(@"YNNetworking: unable to create session configuration.");
        return nil;
    }
    
    sessionConfiguration.timeoutIntervalForRequest = DEFAULT_TIMEOUT_INTERVAL;
    sessionConfiguration.requestCachePolicy = NSURLRequestUseProtocolCachePolicy;
    
    AFHTTPSessionManager *sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:serverBaseUrl sessionConfiguration:sessionConfiguration];
    sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
    [sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    if (auth)
    {
        NSString *authorizationToken = [[YNUserManager sharedInstance] authorizationToken];
        
        if (!authorizationToken || [authorizationToken isEmpty]) {
            NSLog(@"YNNetworking: authorization token is empty or nil.");
            return nil;
        }
        
        [sessionManager.requestSerializer setValue:authorizationToken forHTTPHeaderField:@"Authorization"];
    }
    
    return sessionManager;
}

+ (void)requestGETWithUrl:(NSString*)urlString
               parameters:(NSDictionary*)params
            authorization:(BOOL)auth
                  success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
                  failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure
{
    AFHTTPSessionManager *sessionManager = [YNNetworking defaultSessionManagerWithAuthorization:auth];
    
    if (!sessionManager)
    {
        NSLog(@"YNNetworking: unable to create session manager.");
        NSError *error = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_VALIDATION code:0 userInfo:nil];
        failure(error, nil);
        return;
    }
    
    [sessionManager GET:urlString
             parameters:params
               progress:nil
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                   
                    [YNNetworking successHandlerWithResponseObject:responseObject taskResponse:task.response success:success];
                    
               } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                   
                   [YNNetworking failureHandlerWithError:error taskResponse:task.response failure:failure];
                   
               }];
}

+ (void)requestPOSTWithUrl:(NSString*)urlString
                parameters:(NSDictionary*)params
             authorization:(BOOL)auth
                   success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
                   failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure
{
    AFHTTPSessionManager *sessionManager = [YNNetworking defaultSessionManagerWithAuthorization:auth];
    
    if (!sessionManager)
    {
        NSLog(@"YNNetworking: unable to create session manager.");
        NSError *error = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_VALIDATION code:0 userInfo:nil];
        failure(error, nil);
        return;
    }
    
    [sessionManager POST:urlString
              parameters:params
                progress:nil
                 success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                     
                     [YNNetworking successHandlerWithResponseObject:responseObject taskResponse:task.response success:success];
                     
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                    [YNNetworking failureHandlerWithError:error taskResponse:task.response failure:failure];

                }];
}

+ (void)requestPUTWithUrl:(NSString*)urlString
               parameters:(NSDictionary*)params
            authorization:(BOOL)auth
                  success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
                  failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure
{
    AFHTTPSessionManager *sessionManager = [YNNetworking defaultSessionManagerWithAuthorization:auth];
    
    if (!sessionManager)
    {
        NSLog(@"YNNetworking: unable to create session manager.");
        NSError *error = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_VALIDATION code:0 userInfo:nil];
        failure(error, nil);
        return;
    }
    
    [sessionManager PUT:urlString
             parameters:params
                success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    
                    [YNNetworking successHandlerWithResponseObject:responseObject taskResponse:task.response success:success];
                    
                } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                    
                    [YNNetworking failureHandlerWithError:error taskResponse:task.response failure:failure];
                    
                }];
}

+ (void)requestDELETEWithUrl:(NSString*)urlString
                  parameters:(NSDictionary*)params
               authorization:(BOOL)auth
                     success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
                     failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure
{
    AFHTTPSessionManager *sessionManager = [YNNetworking defaultSessionManagerWithAuthorization:auth];
    
    if (!sessionManager)
    {
        NSLog(@"YNNetworking: unable to create session manager.");
        NSError *error = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_VALIDATION code:0 userInfo:nil];
        failure(error, nil);
        return;
    }
    
    [sessionManager DELETE:urlString
                parameters:params
                   success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                    
                       [YNNetworking successHandlerWithResponseObject:responseObject taskResponse:task.response success:success];
                       
                   } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                       
                       [YNNetworking failureHandlerWithError:error taskResponse:task.response failure:failure];
                       
                   }];
}

+ (void)successHandlerWithResponseObject:(id)responseObject
                            taskResponse:(NSURLResponse*)taskResponse
                                 success:(void (^)(NSDictionary *responseObject, NSHTTPURLResponse *httpResponse))success
{
    NSHTTPURLResponse *httpTaskResponse = (NSHTTPURLResponse*)taskResponse;
    
    NSInteger code = httpTaskResponse.statusCode;
    NSString *pathURLString = httpTaskResponse.URL.path;
    
    if (code < 200) {
        NSLog(@"YNNetworking: \"%@\". Informational (%ld).", pathURLString, (long)code);
    }
    
    if (code >= 200 && code < 300) {
        NSLog(@"YNNetworking: \"%@\". Success (%ld).", pathURLString, (long)code);
    }
    
    if (code >= 300 && code < 400) {
        NSLog(@"YNNetworking: \"%@\". Redirection (%ld).", pathURLString, (long)code);
    }
    
    if (code >= 400 && code < 500) {
        NSLog(@"YNNetworking: \"%@\". Client Error (%ld).", pathURLString, (long)code);
    }
    
    if (code >= 500) {
        NSLog(@"YNNetworking: \"%@\". Server Error (%ld).", pathURLString, (long)code);
    }
    
    if (success) {
        success(responseObject, httpTaskResponse);
    }
}

+ (void)failureHandlerWithError:(NSError *)error
                   taskResponse:(NSURLResponse *)taskResponse
                        failure:(void (^)(NSError *error, NSHTTPURLResponse *httpResponse))failure
{
    @try {
        
        NSHTTPURLResponse *httpTaskResponse = (NSHTTPURLResponse*)taskResponse;
        
        if (taskResponse)
        {
            NSInteger code = httpTaskResponse.statusCode;
            NSString *pathURLString = httpTaskResponse.URL.path;
            
            NSLog(@"YNNetworking: \"%@\". Failure (%ld).", pathURLString, (long)code);
        } else {
            NSLog(@"YNNetworking: pathURLString is nil. Failure (%ld).", (long)error.code);
        }
        
        
        NSError *networkingError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_NETWORKING code:httpTaskResponse.statusCode userInfo:error.userInfo];
        
        if (failure) {
            failure(networkingError, httpTaskResponse);
        }

    }
    @catch (NSException *exception) {
        
        NSError *excError = [NSError errorWithDomain:YENOT_ERROR_DOMAIN_APPLICATION code:0 userInfo:nil];
        
        if (failure) {
            failure(excError, nil);
        }
        
    }
}

@end
