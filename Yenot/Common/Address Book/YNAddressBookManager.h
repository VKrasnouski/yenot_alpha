//
//  YNAddressBookManager.h
//  Yenot
//
//  Created by FanLee on 29.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import "YNModelAddressBookPerson.h"

@interface YNAddressBookManager : NSObject

@property (nonatomic, assign) ABAddressBookRef addressBookRef;

+ (instancetype)sharedInstance;

- (NSArray *)getAllPersons;

- (NSArray *)getAllPhoneNumbers;

- (YNModelAddressBookPerson *)getContactWithPhoneNumber:(NSNumber *)phoneNumber;

@end
