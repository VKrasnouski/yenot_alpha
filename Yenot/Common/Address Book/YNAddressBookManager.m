//
//  YNAddressBookManager.m
//  Yenot
//
//  Created by FanLee on 29.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNAddressBookManager.h"

static YNAddressBookManager *addressBookManager;

@implementation YNAddressBookManager

+ (instancetype)sharedInstance
{
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        addressBookManager = [[YNAddressBookManager alloc] init];
        
        // Создаем главный экземпляр адресной книги
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        if (!addressBookRef) {
            addressBookManager.addressBookRef = nil;
        }
        
        // Запрашиваем статус доступа к адресной книге
        if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
        {
            // Если пользователь еще не давал ответ на разрешение доступа к адресной книге
            // Запрашиваем разрешение доступа
            ABAddressBookRequestAccessWithCompletion(addressBookRef, ^(bool granted, CFErrorRef error)
            {
                if (granted) {
                    // Пользователь разрешил доступ к адресной книге
                    addressBookManager.addressBookRef = addressBookRef;
                } else {
                    // Пользователь запретил доступ к адресной книге
                }
            });
        } else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
        {
            // Если пользователь уже давал доступ к адресной книге
            addressBookManager.addressBookRef = addressBookRef;
        } else {
            // Пользователь уже запрещал доступ к адресной книге ранее
        }

    });
    
    return addressBookManager;
}

- (NSArray*)getAllPersons
{
    if (!_addressBookRef) {
        return [NSArray array];
    }
    
    ABRecordRef source = ABAddressBookCopyDefaultSource(_addressBookRef);
    
    NSArray *contacts = (__bridge_transfer NSArray *)(ABAddressBookCopyArrayOfAllPeopleInSourceWithSortOrdering(_addressBookRef, source, kABPersonSortByFirstName));
    
    NSMutableArray *persons = [[NSMutableArray alloc] init];
    
    for (NSUInteger i = 0; i < [contacts count]; i++)
    {
        ABRecordRef contact = (__bridge ABRecordRef)contacts[i];
        
        NSString *firstName = (__bridge_transfer NSString *)ABRecordCopyValue(contact, kABPersonFirstNameProperty);
        NSString *lastName = (__bridge_transfer NSString *)ABRecordCopyValue(contact, kABPersonLastNameProperty);
        ABMultiValueRef numbers = ABRecordCopyValue(contact, kABPersonPhoneProperty);
        NSString *number = (__bridge_transfer NSString *)ABMultiValueCopyValueAtIndex(numbers, 0);
        CFDataRef imageData = ABPersonCopyImageData(contact);
        UIImage *photo = [UIImage imageWithData:(__bridge NSData *)imageData];

        YNModelAddressBookPerson *person = [[YNModelAddressBookPerson alloc] init];
        person.firstName = firstName;
        person.lastName = lastName;
        person.number = number;
        person.photo = photo;
        
        [persons addObject:person];
    }
    
    return persons;
}

- (NSArray*)getAllPhoneNumbers
{
    NSArray *allPersons = [self getAllPersons];
    
    if (allPersons.count > 0)
    {
        NSMutableArray *numbers = [[NSMutableArray alloc] init];
        for (YNModelAddressBookPerson *person in allPersons)
        {
            id number = [person.number convertToPhoneNumber];
            if (number) {
                [numbers addObject:(NSNumber*)number];
            }
        }
        return [NSArray arrayWithArray:numbers];
    } else {
        return [NSArray array];
    }
}

- (nullable YNModelAddressBookPerson *)getContactWithPhoneNumber:(NSNumber *)phoneNumber
{
    NSArray *allPersons = [self getAllPersons];
    
    for (YNModelAddressBookPerson *contact in allPersons)
    {
        if ([[contact.number convertToPhoneNumber] isEqualToNumber:phoneNumber]) {
            return contact;
        }
    }
    
    return nil;
}

@end
