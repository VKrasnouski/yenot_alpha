//
//  YNModelAddressBookPerson.h
//  Yenot
//
//  Created by FanLee on 01.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNModelAddressBookPerson : NSObject

// Имя
@property (nonatomic, copy) NSString *firstName;

// Фамилия
@property (nonatomic, copy) NSString *lastName;

// Телефонный номер
@property (nonatomic, copy) NSString *number;

// Фото
@property (nonatomic, strong) UIImage *photo;

@end
