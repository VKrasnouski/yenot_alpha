//
//  YNModelAddressBookPerson.m
//  Yenot
//
//  Created by FanLee on 01.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNModelAddressBookPerson.h"

@implementation YNModelAddressBookPerson

- (instancetype)init
{
    if (self = [super init])
    {
        _firstName = @"";
        _lastName = @"";
        _number = @"";
        _photo = nil;
    }
    
    return self;
}

@end
