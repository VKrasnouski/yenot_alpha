//
//  YNNewEventRealm.h
//  Yenot
//
//  Created by FanLee on 22/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

//#import "RLMObject.h"
#import "YNEventRealm.h"
#import "YNEventDateRealm.h"



@interface YNNewEventRealm : RLMObject

@property YNEventRealm *event;
@property YNEventDateRealm *eventDate;

-(id)initWithObjects: (YNEventRealm *)event eventDate:(YNEventDateRealm *)eventDate;

@end
