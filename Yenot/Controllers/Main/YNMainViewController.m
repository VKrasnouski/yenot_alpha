//
//  YNMainViewController.m
//  Yenot
//
//  Created by FanLee on 12.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNMainViewController.h"

@interface YNMainViewController ()

@end

@implementation YNMainViewController

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self configureTabBar];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)configureTabBar
{
    [_btnTabBarItem setTitle:@""];
    [_btnTabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    [_btnTabBarItem setImage:[UIImage imageNamed:@"folder"]];
    [_btnTabBarItem setSelectedImage:[UIImage imageNamed:@"folder_hightlighted"]];
}

@end
