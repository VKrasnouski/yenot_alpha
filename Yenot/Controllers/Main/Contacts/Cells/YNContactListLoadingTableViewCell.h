//
//  YNContactListLoadingTableViewCell.h
//  Yenot
//
//  Created by FanLee on 22.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNContactListLoadingTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@end
