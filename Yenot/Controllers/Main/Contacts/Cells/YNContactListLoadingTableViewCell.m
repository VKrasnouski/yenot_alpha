//
//  YNContactListLoadingTableViewCell.m
//  Yenot
//
//  Created by FanLee on 22.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNContactListLoadingTableViewCell.h"

@implementation YNContactListLoadingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
