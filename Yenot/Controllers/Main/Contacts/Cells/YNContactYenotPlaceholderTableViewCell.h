//
//  YNContactYenotPlaceholderTableViewCell.h
//  Yenot
//
//  Created by FanLee on 20.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNContactYenotPlaceholderTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@end
