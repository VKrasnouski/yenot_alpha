//
//  YNContactListTableViewCell.m
//  Yenot
//
//  Created by FanLee on 01.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNContactListTableViewCell.h"

@implementation YNContactListTableViewCell

- (void)awakeFromNib
{
    // Initialization code
    [super awakeFromNib];
    _iconImage.image = [UIImage imageNamed:@"user_placeholder"];
    _iconImage.layer.borderWidth = 0.0f;
    _iconImage.layer.cornerRadius = _iconImage.frame.size.height / 2;
    _iconImage.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
