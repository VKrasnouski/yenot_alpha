//
//  YNContactListTableViewCell.h
//  Yenot
//
//  Created by FanLee on 01.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNContactListTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iconImage;

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@end
