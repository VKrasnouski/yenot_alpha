//
//  YNContactListHeaderTableViewCell.m
//  Yenot
//
//  Created by FanLee on 16.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNContactListHeaderTableViewCell.h"

@implementation YNContactListHeaderTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
