//
//  YNContactListHeaderTableViewCell.h
//  Yenot
//
//  Created by FanLee on 16.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNContactListHeaderTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@property (nonatomic, weak) IBOutlet UILabel *lblContactsCount;

@end
