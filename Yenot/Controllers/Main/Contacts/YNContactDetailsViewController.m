//
//  YNContactDetailsViewController.m
//  Yenot
//
//  Created by FanLee on 22.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNContactDetailsViewController.h"

@interface YNContactDetailsViewController ()

@end

@implementation YNContactDetailsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self configureView];
}

- (void)configureView
{
    NSMutableString *fullName = [[NSMutableString alloc] init];
    
    if ((_person.firstName) && (![_person.firstName isEmpty])) {
        [fullName appendString:_person.firstName];
    }
    
    if ((_person.lastName) && (![_person.lastName isEmpty])) {
        [fullName appendString:@" "];
        [fullName appendString:_person.lastName];
    }
    
    _lblContactName.text = fullName;
    _lblContactPhone.text = _person.number;
    
    _contactImageView.layer.borderWidth = 0.0f;
    _contactImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _contactImageView.layer.cornerRadius = _contactImageView.frame.size.height / 2;
    _contactImageView.clipsToBounds = YES;
    
    if (_person.photo) {
        _contactImageView.image = _person.photo;
    }
}

- (IBAction)btnBackPress:(id)sender
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnSharePress:(id)sender
{
    
}

- (IBAction)btnFavouritesPress:(id)sender
{

}

@end
