//
//  YNContactsListViewController.m
//  Yenot
//
//  Created by FanLee on 29.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNContactsListViewController.h"
#import "YNAddressBookManager.h"
#import "YNModelAddressBookPerson.h"
#import "YNContactListTableViewCell.h"
#import "YNContactListHeaderTableViewCell.h"
#import "YNContactYenotPlaceholderTableViewCell.h"
#import "YNContactDetailsViewController.h"
#import "YNContactListLoadingTableViewCell.h"

@interface YNContactsListViewController ()
{
    NSMutableArray *yenotContacts;
    NSMutableArray *otherContacts;
}

@end

@implementation YNContactsListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
}

#pragma mark - Configure handlers

- (void)configureView
{
    otherContacts = [NSMutableArray arrayWithArray:[[YNAddressBookManager sharedInstance] getAllPersons]];
    yenotContacts = [NSMutableArray array];
    
    [_contactsTableView registerNib:[UINib nibWithNibName:@"YNContactListTableViewCell" bundle:nil] forCellReuseIdentifier:@"contactCell"];
    
    [self updateYenotContacts];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0: {
            return (yenotContacts.count > 0) ? yenotContacts.count : 1;
        }
        case 1: {
            return otherContacts.count;
        }
        default: {
            return 0;
        }
    }
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    YNContactListHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactHeaderCell"];
    
    switch (section) {
        case 0: {
            cell.lblTitle.text = @"Yenot Contact";
            cell.lblContactsCount.text = [NSString stringWithFormat:@"%lu %@", (unsigned long)yenotContacts.count, @"Contacts"];
            return cell;
        }
        case 1: {
            cell.lblTitle.text = @"Other Contact";
            cell.lblContactsCount.text = [NSString stringWithFormat:@"%lu %@", (unsigned long)otherContacts.count, @"Contacts"];
            return cell;
        }
        default: {
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 44.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section) {
        case 0: {
            
            if (yenotContacts.count == 0)
            {
                YNContactYenotPlaceholderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactYenotPlaceholderCell"];
                cell.lblTitle.text = @"You don't have any Yenot friends yet.\nPlease tap this message to invite them.";
                
                return cell;
            }
            
            if (yenotContacts.count > 0)
            {
                YNContactListTableViewCell *cell = [self getYenotContactCellForTableView:tableView forContactAtIndex:indexPath.row];
                
                return cell;
            }
            
            return nil;
        }
        case 1: {
            YNContactListTableViewCell *cell = [self getOtherContactCellForTableView:tableView forContactAtIndex:indexPath.row];
            return cell;
        }
        default: {
            return nil;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        UIStoryboard *contactDetailsStoryboard = [UIStoryboard storyboardWithName:@"YNContactDetailsStoryboard" bundle:nil];
        UINavigationController *navigationController = [contactDetailsStoryboard instantiateInitialViewController];
        
        YNContactDetailsViewController *contactDetailsViewController = (YNContactDetailsViewController*)[navigationController.viewControllers firstObject];
        contactDetailsViewController.person = otherContacts[indexPath.row];
        
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Addition handlers

- (YNContactListTableViewCell*)getYenotContactCellForTableView:(UITableView *)tableView forContactAtIndex:(NSUInteger)contactIndex
{
    YNContactListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell"];
    
    YNModelAddressBookPerson *person = (YNModelAddressBookPerson*)yenotContacts[contactIndex];
    
    NSMutableString *fullName = [[NSMutableString alloc] init];
    
    if ((person.firstName) && (![person.firstName isEmpty])) {
        [fullName appendString:person.firstName];
    }
    
    if ((person.lastName) && (![person.lastName isEmpty])) {
        [fullName appendString:@" "];
        [fullName appendString:person.lastName];
    }
    
    cell.lblTitle.text = fullName;
    
    UIImage *photoImage;
    if (person.photo) {
        photoImage = person.photo;
    } else {
        photoImage = [UIImage imageNamed:@"user_placeholder"];
    }
    
    cell.iconImage.image = photoImage;
    
    return cell;
}

- (YNContactListTableViewCell*)getOtherContactCellForTableView:(UITableView *)tableView forContactAtIndex:(NSUInteger)contactIndex
{
    YNContactListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell"];
    
    YNModelAddressBookPerson *person = (YNModelAddressBookPerson*)otherContacts[contactIndex];
    
    NSMutableString *fullName = [[NSMutableString alloc] init];
    
    if ((person.firstName) && (![person.firstName isEmpty])) {
        [fullName appendString:person.firstName];
    }
    
    if ((person.lastName) && (![person.lastName isEmpty])) {
        [fullName appendString:@" "];
        [fullName appendString:person.lastName];
    }
    
    cell.lblTitle.text = fullName;
    
    UIImage *photoImage;
    if (person.photo) {
        photoImage = person.photo;
    } else {
        photoImage = [UIImage imageNamed:@"user_placeholder"];
    }
    
    cell.iconImage.image = photoImage;
    
    return cell;
}

- (void)removePerson:(nullable YNModelAddressBookPerson*)person fromContacts:(nullable NSMutableArray*)contacts
{
    if (!person || !contacts) {
        return;
    }
    
    for (YNModelAddressBookPerson *contact in contacts)
    {
        if ([person.number isEqualToString:contact.number]) {
            [contacts removeObject:contact];
            break;
        }
    }
    
    return;
}

- (void)removeAllPersonsWhoInYenotContacts:(NSMutableArray*)yenotContactsArray fromOtherContacts:(NSMutableArray*)otherContactsArray
{
    for (YNModelAddressBookPerson *contact in yenotContactsArray)
    {
        [self removePerson:contact fromContacts:otherContactsArray];
    }
}

- (void)addPersonsFromAddressBookWithPhoneNumbers:(NSArray*)numbers toContacts:(NSMutableArray*)contacts
{
    if (!numbers || !contacts) {
        return;
    }
    
    for (NSNumber *number in numbers)
    {
        YNModelAddressBookPerson *contact = [[YNAddressBookManager sharedInstance] getContactWithPhoneNumber:number];
        [contacts addObject:contact];
    }
}

- (void)updateYenotContacts
{
    YNNumbers *numbers = [[YNNumbers alloc] init];
    numbers.numbers = [[YNAddressBookManager sharedInstance] getAllPhoneNumbers];
    
    [YNRequestManager requestPOSTFilterPhoneNumbersForYenotUsers:numbers
                                                         success:^(YNNumbers *yenotNumbers) {

                                                             [self addPersonsFromAddressBookWithPhoneNumbers:yenotNumbers.numbers toContacts:yenotContacts];
                                                             [self removeAllPersonsWhoInYenotContacts:yenotContacts fromOtherContacts:otherContacts];
                                                             
                                                             [self.contactsTableView reloadData];
                                                             
                                                         }
                                                         failure:^(NSError *error) {
                                                             
                                                             NSLog(@"Error to receive yenot contacts. %@", [error description]);
                                                             
                                                         }];
}

@end
