//
//  YNContactsListViewController.h
//  Yenot
//
//  Created by FanLee on 29.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNContactsListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *contactsTableView;

@end
