//
//  YNContactDetailsViewController.h
//  Yenot
//
//  Created by FanLee on 22.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YNModelAddressBookPerson.h"

@interface YNContactDetailsViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIImageView *contactImageView;

@property (nonatomic, weak) IBOutlet UILabel *lblContactName;

@property (nonatomic, weak) IBOutlet UILabel *lblContactPhone;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnBack;


@property (nonatomic, strong) YNModelAddressBookPerson *person;

- (IBAction)btnBackPress:(id)sender;

- (IBAction)btnSharePress:(id)sender;

- (IBAction)btnFavouritesPress:(id)sender;

@end
