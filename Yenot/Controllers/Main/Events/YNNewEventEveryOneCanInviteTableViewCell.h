//
//  YNNewEventEveryOneCanInviteTableViewCell.h
//  Yenot
//
//  Created by FanLee on 04/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNNewEventEveryOneCanInviteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UISwitch *canInviteSwitch;
@property (weak, nonatomic) IBOutlet UIView *timePiecesViews;

@end
