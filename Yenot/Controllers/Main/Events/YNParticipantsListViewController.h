//
//  YNParticipantsListViewController.h
//  Yenot
//
//  Created by FanLee on 23/08/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNParticipantsListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *participantsListTableView;

@end
