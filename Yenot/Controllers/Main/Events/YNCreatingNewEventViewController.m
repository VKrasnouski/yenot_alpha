//
//  YNCreatingNewEventViewController.m
//  Yenot
//
//  Created by FanLee on 13/12/2016.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNCreatingNewEventViewController.h"
#import "YNIncomingEventDateTimeTableViewCell.h"
#import "YNNewEventEventNameTableViewCell.h"
#import "YNNewEventAllDayTableViewCell.h"
#import "YNNewEventParticipantsTableViewCell.h"
#import "YNNewEventEveryOneCanInviteTableViewCell.h"
#import "YNIncomingEventLocationTableViewCell.h"
#import "YNNewEventLocationTableViewCell.h"
#import "YNEventDatePickerViewController.h"
#import "YNEvent.h"
#import "YNEventDate.h"
#import "YNEventRealm.h"
#import "YNEventDateRealm.h"



@interface YNCreatingNewEventViewController ()

@end

@implementation YNCreatingNewEventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    YNEvent *event = [YNEvent new];
    YNEventDate *eventDate = [YNEventDate new];
    YNEventRealm *eventObj = [[YNEventRealm alloc] initWithMantleModel:event];
    YNEventDateRealm *eventDateRObj = [[YNEventDateRealm alloc] initWithMantleModel:eventDate];
    _createdEvent = [[YNNewEventRealm alloc] initWithObjects:eventObj eventDate:eventDateRObj];
    _eventDaysArray = [NSMutableArray new];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    
    if (_createdEvent.eventDate.dateTimeStart && _createdEvent.eventDate.dateTimeEnd) {
        [_tableView reloadData];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.row)
    {
            
        case 0:{
            YNNewEventEventNameTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"eventNameCell" forIndexPath:indexPath];
            
            return cell;
            break;
        }
        case 1:{
            YNNewEventAllDayTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"allDayCell" forIndexPath:indexPath];
            
            return cell;
            break;
        }

        case 2: {
            
            YNIncomingEventDateTimeTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"dateTimeCell" forIndexPath:indexPath];
            
            
            NSDate *dateFrom;
            NSDate *dateTo;
            
            if (!_createdEvent.eventDate.dateTimeStart && !_createdEvent.eventDate.dateTimeStart) {
                
                dateFrom = [NSDate date];
                dateTo = [NSDate date];
            
            }else {
                dateFrom = [NSDate dateWithTimeIntervalSince1970:[_createdEvent.eventDate.dateTimeStart doubleValue]];
                
                dateTo = [NSDate dateWithTimeIntervalSince1970:[_createdEvent.eventDate.dateTimeEnd doubleValue]];

//                dateTo = [NSDate dateWithTimeIntervalSince1970:[((YNEventDate *)_createdEvent.eventDate).datetimeEnd unsignedIntegerValue]];

            }
            
            //[NSDate dateWithTimeIntervalSince1970:[((YNEventDate *)[_participantEvent.eventDates firstObject]).datetimeStart unsignedIntegerValue]];
            
      
            
            
            //[NSDate dateWithTimeIntervalSince1970:[((YNEventDate *)[_participantEvent.eventDates firstObject]).datetimeEnd unsignedIntegerValue]];
            
            NSString *dayMonthYearFrom = [dateFrom getDateStringByFormat:@"MMMM dd YYYY"];
            NSString *timeFrom = [dateFrom getDateStringByFormat:@"HH:mm"];
            NSString *dayMonthYearTo = [dateTo getDateStringByFormat:@"MMMM dd YYYY"];
            NSString *timeTo = [dateTo getDateStringByFormat:@"HH:mm"];
            
            
            cell.lblTimeFrom.text = timeFrom;
            cell.lblTimeTo.text = timeTo;
            cell.lblDateFrom.text = dayMonthYearFrom;
            cell.lblDateTo.text = dayMonthYearTo;
            
            return cell;
            
            break;
        }
        case 3: {
            
            
            YNNewEventParticipantsTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"participantsCell" forIndexPath:indexPath];
//            YNIncomingEventLocationTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
//            
//            cell.lblLocationTitle.text = NSLocalizedString(@"Объект не указан", nil);
//            cell.lblLocationSubtitle.text = NSLocalizedString(@"Местоположение не указано", nil);
//            
//            if ((_participantEvent.event.lat && _participantEvent.event.lon) && (![_participantEvent.event.lat isEqualToNumber:@0] && ![_participantEvent.event.lon isEqualToNumber:@0]))
//            {
//                cell.lblLocationSubtitle.text = [NSString stringWithFormat:@"%@, %@", _participantEvent.event.lat, _participantEvent.event.lon];
//            }
//            
//            if (_participantEvent.event.locationAddress && ![_participantEvent.event.locationAddress isEmpty]) {
//                cell.lblLocationSubtitle.text = _participantEvent.event.locationAddress;
//            }
//            
//            if (_participantEvent.event.locationName && ![_participantEvent.event.locationName isEmpty]) {
//                cell.lblLocationTitle.text = _participantEvent.event.locationName;
//            }
//            
//            [cell configureCellWithLon:_participantEvent.event.lon lat:_participantEvent.event.lat];
//            
            return cell;
            
            break;
        }
        case 4: {
            
            YNNewEventEveryOneCanInviteTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"everyOneCanInviteCell" forIndexPath:indexPath];
            
//            YNIncomingEventTimeZoneTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"timeZoneCell" forIndexPath:indexPath];
//            cell.lblTimeZone.text = [NSString stringWithFormat:@"%@ (%@)", _participantEvent.event.timezone, @"UTC"];
            
            return cell;
            
            break;
        }
        case 5: {
            
            
            YNNewEventLocationTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];

//            YNIncomingEventDescriptionTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"descriptionCell" forIndexPath:indexPath];
//            
            if (_createdEvent.event.locationName && ![_createdEvent.event.locationName isEmpty]) {
                cell.titleLabel.text = [NSString stringWithFormat:@"%@, %@", _createdEvent.event.locationName, _createdEvent.event.locationAddress] ;
            } else {
                //cell.titleLabel.text = NSLocalizedString(@"Описание для события не задано", nil);
                //[cell.titleLabel setTextColor:[UIColor lightGrayColor]];
            }
            
            [cell configureCellWithLon:_createdEvent.event.lon lat:_createdEvent.event.lat];

            
            return cell;
//            
            break;
        }
            
            case 6: {
                
                
                UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"noteCell" forIndexPath:indexPath];
                //noteCell
            
                return cell;
                
                break;

            }
        case 7: {
            
            
            UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"periodCell" forIndexPath:indexPath];
            //noteCell
            
            return cell;
            
            break;
            
        }

            
        default: {
            
//            YNContactListTableViewCell *cell = [self getContactCellForTableView:_tableView forNumber:_participantEvent.organizerNumbers[indexPath.row - 5]];
//            
//            return cell;
//            
            break;
        }
    }
    
    return nil;

}

#pragma mark - TableView Delegates


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 2 || indexPath.row == 5) {
        return  120;
    }
    return 44;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 5) {
        
        GMSPlacePickerConfig *config = [[GMSPlacePickerConfig alloc] initWithViewport:nil];
        GMSPlacePickerViewController *placePicker =
        [[GMSPlacePickerViewController alloc] initWithConfig:config];
        placePicker.delegate = self;
        
        [self presentViewController:placePicker animated:YES completion:nil];

    }
}

#pragma mark GMSPlacePickerDelegates

- (void)placePicker:(GMSPlacePickerViewController *)viewController didPickPlace:(GMSPlace *)place {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    _createdEvent.event.locationName = place.name;
    _createdEvent.event.locationAddress = place.formattedAddress;
    NSLog(@"Place name %@", place.name);
    NSLog(@"Place address %@", place.formattedAddress);
    NSLog(@"Place attributions %@", place.attributions.string);
    _createdEvent.event.lon = @(place.coordinate.longitude);
    _createdEvent.event.lat = @(place.coordinate.latitude);
    _createdEvent.event.eventPlaceUID = place.placeID;
    [_tableView reloadData];
}

- (void)placePickerDidCancel:(GMSPlacePickerViewController *)viewController {
    // Dismiss the place picker, as it cannot dismiss itself.
    [viewController dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"No place selected");
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.destinationViewController isKindOfClass:[YNEventDatePickerViewController class]]) {
        YNEventDatePickerViewController *dateVC = segue.destinationViewController;
        dateVC.eventDate = _eventDaysArray;
        dateVC.eventDateRObj = _createdEvent.eventDate;

    }
    

    //showCalendarVC
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
