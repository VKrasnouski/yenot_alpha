//
//  YNEventTimePickerViewController.h
//  Yenot
//
//  Created by FanLee on 27/03/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNEventTimePickerViewController : UIViewController <UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIDatePicker *beginTimePicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *endTimePicker;
@property (assign, nonatomic) NSTimeInterval beginTimeInterval;
@property (assign, nonatomic) NSTimeInterval endTimeInterval;
@property (strong, nonatomic) NSMutableDictionary *timeIntervals;

@end
