//
//  YNEventDatePickerViewController.m
//  Yenot
//
//  Created by FanLee on 02/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import "YNEventDatePickerViewController.h"
#import "FSCalendar/FSCalendar.h"
#import "YNEventTimePickerViewController.h"


@interface YNEventDatePickerViewController ()

@property (strong, nonatomic) NSDate *minimumDate;
@property (strong, nonatomic) NSDate *maximumDate;
@property (strong, nonatomic) NSCalendar *gregorian;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;




@end

@implementation YNEventDatePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _timeIntervals = [NSMutableDictionary new];
    
    
    
    self.gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd";
    
    self.minimumDate = [self.dateFormatter dateFromString:@"2017-01-01"];
    self.maximumDate = [self.dateFormatter dateFromString:@"2028-12-31"];
    //FSCalendar *calendar = [[FSCalendar alloc] initWithFrame:CGRectMake(0, 0, 320, 300)];
    
        
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - View Actions

- (IBAction)backButtonTapped:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewWillAppear:(BOOL)animated{
    if ([[_timeIntervals allKeys] count] > 0) {
        NSNumber *firstobject =  _eventDate[0];
        firstobject = @(firstobject.doubleValue + [[_timeIntervals valueForKey:@"beginTime"] doubleValue]);
        [_eventDate removeObjectAtIndex:0];
        [_eventDate insertObject:firstobject atIndex:0];
        _eventDateRObj.dateTimeStart = _eventDate[0];
        
        NSNumber *lastObject = [_eventDate lastObject];
        lastObject = @(lastObject.doubleValue + [[_timeIntervals valueForKey:@"endTime"] doubleValue]);
        [_eventDate removeLastObject];
        [_eventDate addObject:lastObject];
        _eventDateRObj.dateTimeEnd = [_eventDate lastObject];
        
    }
    
}



#pragma mark - FSCalendarDataSource

- (NSDate *)minimumDateForCalendar:(FSCalendar *)calendar
{
    return self.minimumDate;
}

- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar
{
    return self.maximumDate;
}

//- (NSString *)calendar:(FSCalendar *)calendar subtitleForDate:(NSDate *)date
//{
//    if (_showsEvents) {
//        EKEvent *event = [self eventsForDate:date].firstObject;
//        if (event) {
//            return event.title;
//        }
//    }
//    if (_showsLunar) {
//        NSInteger day = [_lunarCalendar component:NSCalendarUnitDay fromDate:date];
//        return _lunarChars[day-1];
//    }
//    return nil;
//}


#pragma mark - FSCalendarDelegate

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    NSLog(@"did select %@",[self.dateFormatter stringFromDate:date]);
    
    NSDate *dayBeginingOfADay = [self dateAtBeginningOfDayForDate:date];
    [_eventDate addObject:[self getTimeInterval:dayBeginingOfADay]];
}

- (void)calendarCurrentPageDidChange:(FSCalendar *)calendar
{
    NSLog(@"did change page %@",[self.dateFormatter stringFromDate:calendar.currentPage]);
}

-(void)calendar:(FSCalendar *)calendar didDeselectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition{
    
    NSLog(@"did deselect %@",[self.dateFormatter stringFromDate:date]);
   
    NSDate *dayBeginingOfADay = [self dateAtBeginningOfDayForDate:date];
    [_eventDate removeObject:[self getTimeInterval:dayBeginingOfADay]];
    
}

-(NSNumber*)getTimeInterval:(NSDate*)incomingDate{
    
    NSTimeInterval timeInterval = [incomingDate timeIntervalSince1970];
    NSNumber *timeNumber = [NSNumber numberWithDouble:timeInterval];

    return timeNumber;
}


- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}

/*
- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    if (!self.showsEvents) return 0;
    if (!self.events) return 0;
    NSArray<EKEvent *> *events = [self eventsForDate:date];
    return events.count;
}

- (NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventDefaultColorsForDate:(NSDate *)date
{
    if (!self.showsEvents) return nil;
    if (!self.events) return nil;
    NSArray<EKEvent *> *events = [self eventsForDate:date];
    NSMutableArray<UIColor *> *colors = [NSMutableArray arrayWithCapacity:events.count];
    [events enumerateObjectsUsingBlock:^(EKEvent * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [colors addObject:[UIColor colorWithCGColor:obj.calendar.CGColor]];
    }];
    return colors.copy;
}
 */



//- (void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:YES];
//    
//    NSDate *today = [NSDate date];
//    
//    NSDate *beginDate1 = [GLDateUtils dateByAddingDays:-32 toDate:today];
//    NSDate *endDate1 = [GLDateUtils dateByAddingDays:-26 toDate:today];
//    GLCalendarDateRange *range1 = [GLCalendarDateRange rangeWithBeginDate:beginDate1 endDate:endDate1];
//    range1.backgroundColor = UIColorFromRGB(0x79a9cd);
//    range1.editable = YES;
//    
//    NSDate *beginDate2 = [GLDateUtils dateByAddingDays:-6 toDate:today];
//    NSDate *endDate2 = [GLDateUtils dateByAddingDays:-3 toDate:today];
//    GLCalendarDateRange *range2 = [GLCalendarDateRange rangeWithBeginDate:beginDate2 endDate:endDate2];
//    range2.backgroundColor = UIColorFromRGB(0x79a9cd);
//    range2.editable = YES;
//    
//    self.calendarView.ranges = [@[range1, range2] mutableCopy];
//    
//    [self.calendarView reload];
//    dispatch_async(dispatch_get_main_queue(), ^{
//        [self.calendarView scrollToDate:self.calendarView.lastDate animated:NO];
//    });
//}
//

#pragma mark - Calendar selectors


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.destinationViewController isKindOfClass:[YNEventTimePickerViewController class]]) {
        YNEventTimePickerViewController *vc = [segue destinationViewController];
        vc.timeIntervals = _timeIntervals;
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
