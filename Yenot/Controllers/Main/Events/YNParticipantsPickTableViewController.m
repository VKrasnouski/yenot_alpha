//
//  YNParticipantsPickTableViewController.m
//  Yenot
//
//  Created by FanLee on 22/08/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import "YNParticipantsPickTableViewController.h"
#import "YNAddressBookManager.h"

@interface YNParticipantsPickTableViewController (){
    NSMutableArray *yenotContacts;
    NSMutableArray *otherContacts;
}

@end

@implementation YNParticipantsPickTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    otherContacts = [NSMutableArray arrayWithArray:[[YNAddressBookManager sharedInstance] getAllPersons]];
    yenotContacts = [NSMutableArray array];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 0;
}

#pragma mark - Contact List

- (void)removePerson:(nullable YNModelAddressBookPerson*)person fromContacts:(nullable NSMutableArray*)contacts
{
    if (!person || !contacts) {
        return;
    }
    
    for (YNModelAddressBookPerson *contact in contacts)
    {
        if ([person.number isEqualToString:contact.number]) {
            [contacts removeObject:contact];
            break;
        }
    }
    
    return;
}


- (void)removeAllPersonsWhoInYenotContacts:(NSMutableArray*)yenotContactsArray fromOtherContacts:(NSMutableArray*)otherContactsArray
{
    for (YNModelAddressBookPerson *contact in yenotContactsArray)
    {
        [self removePerson:contact fromContacts:otherContactsArray];
    }
}

- (void)addPersonsFromAddressBookWithPhoneNumbers:(NSArray*)numbers toContacts:(NSMutableArray*)contacts
{
    if (!numbers || !contacts) {
        return;
    }
    
    for (NSNumber *number in numbers)
    {
        YNModelAddressBookPerson *contact = [[YNAddressBookManager sharedInstance] getContactWithPhoneNumber:number];
        [contacts addObject:contact];
    }
}



- (void)updateYenotContacts
{
    YNNumbers *numbers = [[YNNumbers alloc] init];
    numbers.numbers = [[YNAddressBookManager sharedInstance] getAllPhoneNumbers];
    
    [YNRequestManager requestPOSTFilterPhoneNumbersForYenotUsers:numbers
                                                         success:^(YNNumbers *yenotNumbers) {
                                                             
                                                             [self addPersonsFromAddressBookWithPhoneNumbers:yenotNumbers.numbers toContacts:yenotContacts];
                                                             [self removeAllPersonsWhoInYenotContacts:yenotContacts fromOtherContacts:otherContacts];
                                                             
                                                             //[self.contactsTableView reloadData];
                                                             
                                                         }
                                                         failure:^(NSError *error) {
                                                             
                                                             NSLog(@"Error to receive yenot contacts. %@", [error description]);
                                                             
                                                         }];
}


/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
