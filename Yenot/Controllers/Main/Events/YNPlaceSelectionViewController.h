//
//  YNPlaceSelectionViewController.h
//  Yenot
//
//  Created by FanLee on 04/09/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GooglePlacePicker;

@interface YNPlaceSelectionViewController : UIViewController<GMSPlacePickerViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *selectPlaceButton;
@property (weak, nonatomic) IBOutlet UIView *mapView;

@end
