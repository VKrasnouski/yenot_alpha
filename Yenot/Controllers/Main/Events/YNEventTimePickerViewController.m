//
//  YNEventTimePickerViewController.m
//  Yenot
//
//  Created by FanLee on 27/03/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import "YNEventTimePickerViewController.h"

@interface YNEventTimePickerViewController (){
    
    NSDate *openDate;
}

@end

@implementation YNEventTimePickerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    openDate = [NSDate date];
    _beginTimePicker.date = openDate;
    _endTimePicker.date = openDate;
    
    openDate = [self dateAtBeginningOfDayForDate:openDate];
    
    //    _beginTimePicker.date = openDate;
    //    _endTimePicker.date = openDate;
    
    //    [_beginTimePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    //    [_endTimePicker setValue:[UIColor whiteColor] forKeyPath:@"textColor"];
    //    SEL selector = NSSelectorFromString( @"setHighlightsToday:" );
    //    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature :
    //                                [UIDatePicker
    //                                 instanceMethodSignatureForSelector:selector]];
    //    BOOL no = NO;
    //    [invocation setSelector:selector];
    //    [invocation setArgument:&no atIndex:2];
    //    [invocation invokeWithTarget:_beginTimePicker];
    //    [invocation invokeWithTarget:_endTimePicker];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backButtonPressed:(id)sender {
    
    NSDate *beginDate = _beginTimePicker.date;
    NSDate *endDate = _endTimePicker.date;
    _beginTimeInterval = [beginDate timeIntervalSinceDate:openDate];
    _endTimeInterval = [endDate timeIntervalSinceDate:openDate];
    NSNumber *beginTimeInterval = @(_beginTimeInterval);
    NSNumber *endTimeInterval = @(_endTimeInterval);
    
    [_timeIntervals setObject:beginTimeInterval forKey:@"beginTime"];
    [_timeIntervals setObject:endTimeInterval forKey:@"endTime"];
    
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSDate *)dateAtBeginningOfDayForDate:(NSDate *)inputDate
{
    // Use the user's current calendar and time zone
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSTimeZone *timeZone = [NSTimeZone systemTimeZone];
    [calendar setTimeZone:timeZone];
    
    // Selectively convert the date components (year, month, day) of the input date
    NSDateComponents *dateComps = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:inputDate];
    
    // Set the time components manually
    [dateComps setHour:0];
    [dateComps setMinute:0];
    [dateComps setSecond:0];
    
    // Convert back
    NSDate *beginningOfDay = [calendar dateFromComponents:dateComps];
    return beginningOfDay;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
