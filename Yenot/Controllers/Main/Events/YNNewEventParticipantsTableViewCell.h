//
//  YNNewEventParticipantsTableViewCell.h
//  Yenot
//
//  Created by FanLee on 04/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNNewEventParticipantsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end
