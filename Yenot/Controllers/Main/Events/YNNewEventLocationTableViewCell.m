//
//  YNNewEventLocationTableViewCell.m
//  Yenot
//
//  Created by FanLee on 04/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import "YNNewEventLocationTableViewCell.h"

static NSString *kGoogleMapsApiKey = @"AIzaSyCQLoHKY-CBLsoZBUTPUYX6bxaAwVTpJ-U";

@implementation YNNewEventLocationTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureCellWithLon:(NSNumber *)lon lat:(NSNumber *)lat
{
    if ((!lon && !lat) || ([lon isEqualToNumber:@0] && [lat isEqualToNumber:@0]))
    {
        //_locationImageTopConstraint.active = NO;
        _locationImageView.hidden = YES;
        
        
        
        return;
    }
    
    NSDictionary *param = @{@"width" : @(_locationImageView.frame.size.width),
                            @"height" : @(_locationImageView.frame.size.height)
                            };
    
//    https://maps.googleapis.com/maps/api/staticmap?center=Brooklyn+Bridge,New+York,NY&zoom=13&size=600x300&maptype=roadmap
//    &markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318
//    &markers=color:red%7Clabel:C%7C40.718217,-73.998284
//    &key=YOUR_API_KEY
    NSString *methodName = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/staticmap?scale=2&size=%@x%@&maptype=terrain", [param objectForKey:@"width"], [param objectForKey:@"height"]];
    

    
    NSString *marker = [NSString stringWithFormat:@"&markers=color:blue%%7Clabel:S%%7C%@,%@", lat, lon];
    methodName = [methodName stringByAppendingString:marker];
    
    [methodName stringByAppendingString:[NSString stringWithFormat:@"&key=%@", kGoogleMapsApiKey]];
    
    NSURL *url = [NSURL URLWithString:methodName];
    
    UIImage *mapImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
    [_locationImageView setImage:mapImage];
}

@end
