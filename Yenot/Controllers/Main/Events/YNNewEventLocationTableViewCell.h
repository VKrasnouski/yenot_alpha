//
//  YNNewEventLocationTableViewCell.h
//  Yenot
//
//  Created by FanLee on 04/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNNewEventLocationTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationViewHeight;
//@property (strong, nonatomic) YNNewEventRealm *newEvent;
@property (weak, nonatomic) IBOutlet UIImageView *locationImageView;


- (void)configureCellWithLon:(NSNumber *)lon lat:(NSNumber *)lat;

@end
