//
//  YNParticipantsListViewController.m
//  Yenot
//
//  Created by FanLee on 23/08/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import "YNParticipantsListViewController.h"
#import "YNAddressBookManager.h"

@interface YNParticipantsListViewController (){
    NSMutableArray *yenotContacts;
    NSMutableArray *otherContacts;
}

@end

@implementation YNParticipantsListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    otherContacts = [NSMutableArray arrayWithArray:[[YNAddressBookManager sharedInstance] getAllPersons]];
    yenotContacts = [NSMutableArray array];
    [self updateYenotContacts];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [otherContacts count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifire = @"partCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifire];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifire];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    YNModelAddressBookPerson *cellPerson = otherContacts[indexPath.row];
    NSString *fullName = [NSString stringWithFormat:@"%@ %@", cellPerson.firstName, cellPerson.lastName];
    cell.textLabel.text = fullName;
    
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
}

#pragma mark - Contact List

- (void)removePerson:(nullable YNModelAddressBookPerson*)person fromContacts:(nullable NSMutableArray*)contacts
{
    if (!person || !contacts) {
        return;
    }
    
    for (YNModelAddressBookPerson *contact in contacts)
    {
        if ([person.number isEqualToString:contact.number]) {
            [contacts removeObject:contact];
            break;
        }
    }
    
    return;
}


- (void)removeAllPersonsWhoInYenotContacts:(NSMutableArray*)yenotContactsArray fromOtherContacts:(NSMutableArray*)otherContactsArray
{
    for (YNModelAddressBookPerson *contact in yenotContactsArray)
    {
        [self removePerson:contact fromContacts:otherContactsArray];
    }
}

- (void)addPersonsFromAddressBookWithPhoneNumbers:(NSArray*)numbers toContacts:(NSMutableArray*)contacts
{
    if (!numbers || !contacts) {
        return;
    }
    
    for (NSNumber *number in numbers)
    {
        YNModelAddressBookPerson *contact = [[YNAddressBookManager sharedInstance] getContactWithPhoneNumber:number];
        [contacts addObject:contact];
    }
}



- (void)updateYenotContacts
{
    YNNumbers *numbers = [[YNNumbers alloc] init];
    numbers.numbers = [[YNAddressBookManager sharedInstance] getAllPhoneNumbers];
    
    [YNRequestManager requestPOSTFilterPhoneNumbersForYenotUsers:numbers
                                                         success:^(YNNumbers *yenotNumbers) {
                                                             
                                                             [self addPersonsFromAddressBookWithPhoneNumbers:yenotNumbers.numbers toContacts:yenotContacts];
                                                             [self removeAllPersonsWhoInYenotContacts:yenotContacts fromOtherContacts:otherContacts];
                                                             
                                                             [self.participantsListTableView reloadData];
                                                             
                                                         }
                                                         failure:^(NSError *error) {
                                                             
                                                             NSLog(@"Error to receive yenot contacts. %@", [error description]);
                                                             
                                                         }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
