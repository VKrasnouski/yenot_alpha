//
//  YNEventDatePickerViewController.h
//  Yenot
//
//  Created by FanLee on 02/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YNEventDate.h"
#import "FSCalendar/FSCalendar.h"
#import "YNEventDateRealm.h"



typedef NS_ENUM(NSUInteger, YNSelectedDateState){
        YNSelectedDateStateStart = 0,
        YNSelectedDateStateEnd= 1
};


@interface YNEventDatePickerViewController : UIViewController <FSCalendarDataSource, FSCalendarDelegate>

@property (nonatomic, strong) YNEventDateRealm *eventDateRObj;
@property (nonatomic, strong) NSMutableArray *eventDate;
@property (nonatomic, strong) NSMutableDictionary *timeIntervals;
@property (weak, nonatomic) IBOutlet FSCalendar *calendar;


@end
