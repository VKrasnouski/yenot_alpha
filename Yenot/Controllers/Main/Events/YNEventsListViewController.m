//
//  YNEventsListViewController.m
//  Yenot
//
//  Created by FanLee on 29.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNEventsListViewController.h"
#import "YNEventsListEventTableViewCell.h"
#import "YNIncomingEventDetailsViewController.h"
#import "YNCreatingNewEventViewController.h"


@interface YNEventsListViewController ()

@property (nonatomic, strong) YNEvents *events;

@end

@implementation YNEventsListViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        // Perform all initializations here
        _events = [[YNEvents alloc] init];
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self configureTabBar];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateYenotRelatedEvents];
}

- (void)configureTabBar
{
    [_btnTabBarItem setTitle:@""];
    [_btnTabBarItem setImageInsets:UIEdgeInsetsMake(5, 0, -5, 0)];
    [_btnTabBarItem setImage:[UIImage imageNamed:@"events"]];
    [_btnTabBarItem setSelectedImage:[UIImage imageNamed:@"events_hightlighted"]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _events.events.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80.0f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    YNEventsListEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"eventCell"];
    
    YNParticipantEvent* event = _events.events[indexPath.row];

    [cell configureWithEvent:event];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard *incomingEventDetailsStoryboard = [UIStoryboard storyboardWithName:@"YNIncomingEventDetailsStoryboard" bundle:nil];
    YNIncomingEventDetailsViewController *incomingEventDetailsViewController = [incomingEventDetailsStoryboard instantiateInitialViewController];
    incomingEventDetailsViewController.participantEvent = _events.events[indexPath.row];
    
    [self presentViewController:incomingEventDetailsViewController animated:YES completion:nil];
}

#pragma mark - Buttons handlers

- (IBAction)btnAddPress:(id)sender
{
    UIStoryboard *incomingEventDetailsStoryboard = [UIStoryboard storyboardWithName:@"YNCreatingNewEvent" bundle:nil];
    YNCreatingNewEventViewController *creatingNewEventViewController = [incomingEventDetailsStoryboard instantiateInitialViewController];
    
    [self presentViewController:creatingNewEventViewController animated:YES completion:nil];

}

- (IBAction)btnGroupPress:(id)sender
{

}

- (void)updateYenotRelatedEvents
{
    [YNRequestManager requestGETNewParticipationsIDsWithSuccess:^(YNIDs *ids) {
        
        [YNRequestManager requestGETEventsWithParticipationsIDs:ids success:^(YNEvents *events) {
            
            _events = events;
            
            [_tableView reloadData];
            
        } failure:^(NSError *error) {
            
        }];
        
    } failure:^(NSError *error) {
        
        
    }];
}

@end
