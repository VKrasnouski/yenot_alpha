//
//  YNEventsListEventTableViewCell.m
//  Yenot
//
//  Created by FanLee on 30.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNEventsListEventTableViewCell.h"
#import "YNAddressBookManager.h"

@implementation YNEventsListEventTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)configureWithEvent:(YNParticipantEvent *)event
{
    NSMutableArray *names = [[NSMutableArray alloc] init];
    
    for (NSNumber *number in event.organizerNumbers)
    {
        YNModelAddressBookPerson *person = [[YNAddressBookManager sharedInstance] getContactWithPhoneNumber:number];
        NSString *personName = [NSString stringWithFormat:@"%@ %@", (person.firstName) ?  person.firstName : @"", (person.lastName) ? person.lastName : @""];
        personName = [personName trimmingWhitespaceAndNewlineCharacters];
        
        if ([personName isEmpty]) {
            personName = [number stringValue];
        }
        
        [names addObject:personName];
    }
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:[((YNEventDate *)[event.eventDates firstObject]).datetimeStart unsignedIntegerValue]];
    
    NSString *year = [date getDateStringByFormat:@"YYYY"];
    NSString *dayMonth = [date getDateStringByFormat:@"dd MMM"];
    NSString *time = [date getDateStringByFormat:@"HH:mm"];
   
    _lblYear.text = year;
    _lblDay.text = dayMonth;
    _lblTime.text = time;
    
    _lblDetails.text = NSLocalizedString(@"Местоположение не указано", nil);
    
    if ((event.event.lat && event.event.lon) && (![event.event.lat isEqualToNumber:@0] && ![event.event.lon isEqualToNumber:@0])) {
        _lblDetails.text = [NSString stringWithFormat:@"%@, %@", event.event.lat, event.event.lon];
    }

    if (event.event.locationAddress && ![event.event.locationAddress isEmpty]) {
        _lblDetails.text = event.event.locationAddress;
    }
    
    if (event.event.locationName && ![event.event.locationName isEmpty]) {
        _lblDetails.text = event.event.locationName;
    }
    
    _lblPersonName.text = [names componentsJoinedByString:@", "];
    _lblTitle.text = event.event.name;
}

@end
