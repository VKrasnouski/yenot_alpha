//
//  YNEventsListEventTableViewCell.h
//  Yenot
//
//  Created by FanLee on 30.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNEventsListEventTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTime;

@property (nonatomic, weak) IBOutlet UILabel *lblDay;

@property (nonatomic, weak) IBOutlet UILabel *lblYear;

@property (nonatomic, weak) IBOutlet UILabel *lblPersonName;

@property (nonatomic, weak) IBOutlet UILabel *lblTitle;

@property (nonatomic, weak) IBOutlet UILabel *lblDetails;

@property (nonatomic, weak) IBOutlet UIImageView *personIconImage;

@property (nonatomic, weak) IBOutlet UIImageView *markIconImage;

- (void)configureWithEvent:(YNParticipantEvent *)event;

@end
