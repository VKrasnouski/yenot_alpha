//
//  YNCreatingNewEventViewController.h
//  Yenot
//
//  Created by FanLee on 13/12/2016.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YNNewEventRealm.h"
 @import GooglePlacePicker;


@interface YNCreatingNewEventViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, GMSPlacePickerViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *headerLabel;
@property (strong, nonatomic) YNNewEventRealm *createdEvent;
@property (strong, nonatomic) NSMutableArray *eventDaysArray;
@end
