//
//  YNIncomingEventDescriptionTableViewCell.h
//  Yenot
//
//  Created by FanLee on 28.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNIncomingEventDescriptionTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblDescription;

@end
