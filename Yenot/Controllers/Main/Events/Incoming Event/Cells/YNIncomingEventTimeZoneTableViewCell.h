//
//  YNIncomingEventTimeZoneTableViewCell.h
//  Yenot
//
//  Created by FanLee on 28.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNIncomingEventTimeZoneTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTimeZone;

@end
