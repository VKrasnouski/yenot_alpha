//
//  YNIncomingEventLocationTableViewCell.m
//  Yenot
//
//  Created by FanLee on 27.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNIncomingEventLocationTableViewCell.h"

static NSString *kGoogleMapsApiKey = @"AIzaSyCQLoHKY-CBLsoZBUTPUYX6bxaAwVTpJ-U";

@interface YNIncomingEventLocationTableViewCell ()

@property (nonatomic, strong) NSNumber *longitude;

@property (nonatomic, strong) NSNumber *latitude;

@end

@implementation YNIncomingEventLocationTableViewCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    _longitude = @0;
    _latitude = @0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)configureCellWithLon:(NSNumber *)lon lat:(NSNumber *)lat
{
    if ((!lon && !lat) || ([lon isEqualToNumber:@0] && [lat isEqualToNumber:@0]))
    {
        _locationImageTopConstraint.active = NO;
        _locationImageView.hidden = YES;
        
        
        
        return;
    }
    
    NSDictionary *param = @{@"width" : @(_locationImageView.frame.size.width),
                            @"height" : @(_locationImageView.frame.size.height)
                            };
    
    NSString *methodName = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/staticmap?scale=2&size=%@x%@&maptype=terrain", [param objectForKey:@"width"], [param objectForKey:@"height"]];

    NSString *marker = [NSString stringWithFormat:@"&markers=color:blue%%7Clabel:S%%7C%@,%@", lat, lon];
    methodName = [methodName stringByAppendingString:marker];

    [methodName stringByAppendingString:[NSString stringWithFormat:@"&key=%@", kGoogleMapsApiKey]];
    
    NSURL *url = [NSURL URLWithString:methodName];
    
    UIImage *mapImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:url]];
    
    [_locationImageView setImage:mapImage];
}

@end
