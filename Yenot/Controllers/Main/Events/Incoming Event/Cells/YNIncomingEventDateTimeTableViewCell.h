//
//  YNIncomingEventDateTimeTableViewCell.h
//  Yenot
//
//  Created by FanLee on 23.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNIncomingEventDateTimeTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblTimeFrom;

@property (nonatomic, weak) IBOutlet UILabel *lblTimeTo;

@property (nonatomic, weak) IBOutlet UILabel *lblDateFrom;

@property (nonatomic, weak) IBOutlet UILabel *lblDateTo;

@end
