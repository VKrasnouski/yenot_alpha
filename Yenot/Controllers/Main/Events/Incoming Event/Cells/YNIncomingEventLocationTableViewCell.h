//
//  YNIncomingEventLocationTableViewCell.h
//  Yenot
//
//  Created by FanLee on 27.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNIncomingEventLocationTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *lblLocationTitle;

@property (nonatomic, weak) IBOutlet UILabel *lblLocationSubtitle;

@property (nonatomic, weak) IBOutlet UIImageView *locationImageView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *locationImageTopConstraint;

- (void)configureCellWithLon:(NSNumber *)lon lat:(NSNumber *)lat;

@end
