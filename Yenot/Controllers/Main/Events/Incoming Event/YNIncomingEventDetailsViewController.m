//
//  YNIncomingEventDetailsViewController.m
//  Yenot
//
//  Created by FanLee on 22.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNIncomingEventDetailsViewController.h"
#import "YNIncomingEventDateTimeTableViewCell.h"
#import "YNIncomingEventLocationTableViewCell.h"
#import "YNIncomingEventTimeZoneTableViewCell.h"
#import "YNIncomingEventDescriptionTableViewCell.h"
#import "YNContactListTableViewCell.h"
#import "YNAddressBookManager.h"
#import "YNModelAddressBookPerson.h"

@implementation YNIncomingEventDetailsViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        // Perform all initializations here
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
}

#pragma mark - Configure methods

- (void)configureView
{
    _lblEventTitle.text = _participantEvent.event.name;
    
    [_tableView registerNib:[UINib nibWithNibName:@"YNContactListTableViewCell" bundle:nil] forCellReuseIdentifier:@"contactCell"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 106.0f;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSUInteger personsCount = _participantEvent.organizerNumbers.count;
    
    return 5 + personsCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row)
    {
        case 0: {
            
            YNIncomingEventDateTimeTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"dateTimeCell" forIndexPath:indexPath];
            
            NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:[((YNEventDate *)[_participantEvent.eventDates firstObject]).datetimeStart unsignedIntegerValue]];
            
            NSString *dayMonthYearFrom = [dateFrom getDateStringByFormat:@"MMMM dd YYYY"];
            NSString *timeFrom = [dateFrom getDateStringByFormat:@"HH:mm"];
            
            NSDate *dateTo = [NSDate dateWithTimeIntervalSince1970:[((YNEventDate *)[_participantEvent.eventDates firstObject]).datetimeEnd unsignedIntegerValue]];
            
            NSString *dayMonthYearTo = [dateTo getDateStringByFormat:@"MMMM dd YYYY"];
            NSString *timeTo = [dateTo getDateStringByFormat:@"HH:mm"];
            
            cell.lblTimeFrom.text = timeFrom;
            cell.lblTimeTo.text = timeTo;
            cell.lblDateFrom.text = dayMonthYearFrom;
            cell.lblDateTo.text = dayMonthYearTo;
            
            return cell;
            
            break;
        }
        case 1: {
            
            YNIncomingEventLocationTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"locationCell" forIndexPath:indexPath];
            
            cell.lblLocationTitle.text = NSLocalizedString(@"Объект не указан", nil);
            cell.lblLocationSubtitle.text = NSLocalizedString(@"Местоположение не указано", nil);
            
            if ((_participantEvent.event.lat && _participantEvent.event.lon) && (![_participantEvent.event.lat isEqualToNumber:@0] && ![_participantEvent.event.lon isEqualToNumber:@0]))
            {
                cell.lblLocationSubtitle.text = [NSString stringWithFormat:@"%@, %@", _participantEvent.event.lat, _participantEvent.event.lon];
            }
            
            if (_participantEvent.event.locationAddress && ![_participantEvent.event.locationAddress isEmpty]) {
                cell.lblLocationSubtitle.text = _participantEvent.event.locationAddress;
            }
            
            if (_participantEvent.event.locationName && ![_participantEvent.event.locationName isEmpty]) {
                cell.lblLocationTitle.text = _participantEvent.event.locationName;
            }
            
            [cell configureCellWithLon:_participantEvent.event.lon lat:_participantEvent.event.lat];
            
            return cell;
            
            break;
        }
        case 2: {
            
            YNIncomingEventTimeZoneTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"timeZoneCell" forIndexPath:indexPath];
            cell.lblTimeZone.text = [NSString stringWithFormat:@"%@ (%@)", _participantEvent.event.timezone, @"UTC"];
            
            return cell;
            
            break;
        }
        case 3: {
            
            YNIncomingEventDescriptionTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"descriptionCell" forIndexPath:indexPath];
            
            if (_participantEvent.event.descript && ![_participantEvent.event.descript isEmpty]) {
                cell.lblDescription.text = _participantEvent.event.descript;
            } else {
                cell.lblDescription.text = NSLocalizedString(@"Описание для события не задано", nil);
                [cell.lblDescription setTextColor:[UIColor lightGrayColor]];
            }

            return cell;
            
            break;
        }
        case 4: {
            
            UITableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"personsCell"];
            
            return cell;
            
            break;
        }
            
        default: {
            
            YNContactListTableViewCell *cell = [self getContactCellForTableView:_tableView forNumber:_participantEvent.organizerNumbers[indexPath.row - 5]];
            
            return cell;
            
            break;
        }
    }
    
    return nil;
}

- (YNContactListTableViewCell *)getContactCellForTableView:(UITableView *)tableView forNumber:(NSNumber *)contactNumber
{
    YNContactListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"contactCell"];

    YNModelAddressBookPerson *person = (YNModelAddressBookPerson *)[[YNAddressBookManager sharedInstance] getContactWithPhoneNumber:contactNumber];
    
    NSMutableString *fullName = [[NSMutableString alloc] init];
    
    fullName = [[contactNumber stringValue] mutableCopy];
    
    if ((person.firstName) && (![person.firstName isEmpty])) {
        fullName = [person.firstName mutableCopy];
    }
    
    if ((person.lastName) && (![person.lastName isEmpty])) {
        [fullName appendString:@" "];
        [fullName appendString:person.lastName];
    }
    
    cell.lblTitle.text = [fullName trimmingWhitespaceAndNewlineCharacters];
    
    UIImage *photoImage;
    if (person.photo) {
        photoImage = person.photo;
    } else {
        photoImage = [UIImage imageNamed:@"user_placeholder"];
    }
    
    cell.iconImage.image = photoImage;
    
    return cell;
}

#pragma mark - Buttons handlers

- (IBAction)btnBackPress:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
