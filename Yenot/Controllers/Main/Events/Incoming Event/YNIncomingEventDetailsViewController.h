//
//  YNIncomingEventDetailsViewController.h
//  Yenot
//
//  Created by FanLee on 22.06.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNIncomingEventDetailsViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UILabel *lblEventTitle;

@property (nonatomic, weak) IBOutlet UIButton *btnBack;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) YNParticipantEvent *participantEvent;

- (IBAction)btnBackPress:(id)sender;

@end
