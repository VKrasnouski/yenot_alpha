//
//  YNEventsListViewController.h
//  Yenot
//
//  Created by FanLee on 29.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNEventsListViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UIButton *btnAdd;

@property (nonatomic, weak) IBOutlet UIButton *btnGroup;

@property (nonatomic, weak) IBOutlet UITabBarItem *btnTabBarItem;

@property (nonatomic, weak) IBOutlet UITableView *tableView;

- (IBAction)btnAddPress:(id)sender;

- (IBAction)btnGroupPress:(id)sender;

@end
