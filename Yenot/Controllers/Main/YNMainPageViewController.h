//
//  YNMainPageViewController.h
//  Yenot
//
//  Created by FanLee on 14.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNMainPageViewController : UIPageViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate>

@end
