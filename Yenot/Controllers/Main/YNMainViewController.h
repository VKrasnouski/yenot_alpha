//
//  YNMainViewController.h
//  Yenot
//
//  Created by FanLee on 12.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNMainViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITabBarItem *btnTabBarItem;

@end
