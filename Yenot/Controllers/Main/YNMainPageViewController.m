//
//  YNMainPageViewController.m
//  Yenot
//
//  Created by FanLee on 14.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNMainPageViewController.h"
#import "YNContactsListViewController.h"

@interface YNMainPageViewController ()
{
    NSArray *pages;
}

@end

@implementation YNMainPageViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.delegate = self;
    self.dataSource = self;
    
    UIStoryboard *contactListStoryboard = [UIStoryboard storyboardWithName:@"YNContactsListStoryboard" bundle:nil];
    YNContactsListViewController *contactsListViewController = [contactListStoryboard instantiateInitialViewController];
    
    pages = @[contactsListViewController];
    
    [self setViewControllers:pages direction:UIPageViewControllerNavigationDirectionForward animated:false completion:nil];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [pages indexOfObject:viewController];
    NSUInteger previousIndex = (currentIndex == 0) ? currentIndex : (currentIndex - 1);
    NSLog(@"%@", @(previousIndex));
    
    return nil; //pages[previousIndex];
}

- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger currentIndex = [pages indexOfObject:viewController];
    NSUInteger nextIndex = (currentIndex == (pages.count - 1)) ? currentIndex : (currentIndex + 1);
    NSLog(@"%@", @(nextIndex));
    
    return nil; //pages[nextIndex];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
