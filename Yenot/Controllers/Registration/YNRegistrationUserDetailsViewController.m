//
//  YNRegistrationUserDetailsViewController.m
//  Yenot
//
//  Created by FanLee on 21.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNRegistrationUserDetailsViewController.h"
#import "YNAddressBookManager.h"
#import "YNModelAddressBookPerson.h"

@interface YNRegistrationUserDetailsViewController ()
{
    UIImage *userPhoto;
}

@end

@implementation YNRegistrationUserDetailsViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        // Perform all initializations here
        userPhoto = nil;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Создаем синглтон адресной книги и впервые запрашиваем разрешение
    [YNAddressBookManager sharedInstance];
}

#pragma mark - Configure handlers

- (void)configureView
{
    _btnDone.enabled = FALSE;
    
    _userPhotoImageView.layer.borderWidth = 0.0f;
    _userPhotoImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    _userPhotoImageView.layer.cornerRadius = _userPhotoImageView.frame.size.height / 2;
    _userPhotoImageView.clipsToBounds = YES;
    
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        // В случае если доступ к камере запрещен блокируем доступ к кнопкам
        _btnGallery.enabled = FALSE;
        _btnTakePhoto.enabled = FALSE;
    }
}

- (void)configurePhoto
{
    _userPhotoImageView.image = userPhoto;
}

#pragma mark - Buttons handlers

- (IBAction)btnBackPress:(id)sender
{
    [self registrationBack];
}

- (IBAction)btnDonePress:(id)sender
{
    _btnDone.enabled = NO;
    
    YNDTORegistrationUserData *userData = [[YNDTORegistrationUserData alloc] init];
    userData.nickname = _tfNickName.text;
    userData.password = _userPassword.password;
    userData.code = _smsCode;
    userData.number = _userPhoneNumber.number;
    userData.imei = [self getUDID];
    userData.notificationsApiId = @"0000-0000-0000-0000-0000";
    userData.numbers = [[YNAddressBookManager sharedInstance] getAllPhoneNumbers];
    
    [self requestRegistrationFinishWithPhoneNumber:userData
                                           success:^(YNUserID *userID) {

                                               YNModelUser *user = [[YNModelUser alloc] init];
                                               user.ID = userID.ID;
                                               user.number = userData.number;
                                               user.password = userData.password;
                                               user.nickname = userData.nickname;
                                               user.photo = userPhoto;
                                               
                                               YNUserManager *userManager = [YNUserManager sharedInstance];
                                               [userManager setUser:user];
                                               
                                               _btnDone.enabled = YES;
                                               
                                               [[YNUserManager sharedInstance] saveUser];
                                               
                                               [self registrationFinish];
                                               
                                           }
                                           failure:^(NSError *error) {
                                               
                                               if ([error.domain isEqualToString:YENOT_ERROR_DOMAIN_SERVER])
                                               {
                                                   [UIAlertController showSimpleAlertWithTitle:NSLocalizedString(@"error", nil)
                                                                                       message:NSLocalizedString(@"Registration error", nil)
                                                                              onViewController:self];
                                               }
                                               
                                               if ([error.domain isEqualToString:YENOT_ERROR_DOMAIN_NETWORKING])
                                               {
                                                   [UIAlertController showSimpleAlertWithTitle:NSLocalizedString(@"error", nil)
                                                                                       message:NSLocalizedString(@"Check your network connection please. Wait a few seconds and try again.", nil)
                                                                              onViewController:self];
                                               }
                                               
                                               _btnDone.enabled = YES;
                                           }];
}

- (IBAction)btnGalleryPress:(id)sender
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    pickerController.allowsEditing = YES;
    pickerController.delegate = self;
    
    [self presentViewController:pickerController animated:NO completion:nil];
}

- (IBAction)btnTakePhotoPress:(id)sender
{
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    pickerController.allowsEditing = YES;
    pickerController.showsCameraControls = YES;
    pickerController.delegate = self;
    
    [self presentViewController:pickerController animated:NO completion:nil];
}

#pragma mark - UIImagePickerControllerDelegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *takedOriginalImage = info[UIImagePickerControllerEditedImage];
    
//    CGSize newSize = CGSizeMake(600, 800);
//    
//    UIGraphicsBeginImageContext(newSize);
//    CGContextRef context = UIGraphicsGetCurrentContext();
//    [takedOriginalImage drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
//    CGContextRotateCTM(context, 90 * M_PI / 180);
//    
//    UIImage *rotatedImage = UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
    
    userPhoto = takedOriginalImage;
    [self configurePhoto];
    
    [picker dismissViewControllerAnimated:NO completion:nil];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    _btnDone.enabled = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (resultString && ![resultString isEmpty])
    {
        if (resultString.length > 2) {
            _btnDone.enabled = YES;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    _btnDone.enabled = NO;
    
    return YES;
}

#pragma mark - Addition handlers

- (NSString*)getUDID
{
    return [[[UIDevice currentDevice] identifierForVendor] UUIDString];
}

#pragma mark - Requests

- (void)requestRegistrationFinishWithPhoneNumber:(YNDTORegistrationUserData *)userData
                                         success:(void (^)(YNUserID *userID))success
                                         failure:(void (^)(NSError *error))failure
{
    [YNRequestManager requestPOSTRegistrationFinishSMSWithRegistrationUserData:userData
                                                                       success:^(YNUserID *userID) {
                                                                           success(userID);
                                                                       }
                                                                       failure:^(NSError *error) {
                                                                           failure(error);
                                                                       }];
}

#pragma mark - Keyboard handlers

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - Segues handlers

- (void)registrationBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)registrationFinish
{
    [YNUseCaseManager startMainUseCase];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

}

@end
