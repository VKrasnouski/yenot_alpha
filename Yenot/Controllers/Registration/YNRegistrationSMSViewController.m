//
//  YNRegistrationSMSViewController.m
//  Yenot
//
//  Created by FanLee on 10.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNRegistrationSMSViewController.h"
#import "YNRegistrationUserDetailsViewController.h"

@interface YNRegistrationSMSViewController ()
{
    NSTimer *timer;
    NSInteger timeout;
}

@end

@implementation YNRegistrationSMSViewController

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        // Perform all initializations here
        timeout = 30;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureNavigationBar];
    
    [self configureView];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self startTimer];
}

#pragma mark - Configure handlers

- (void)configureNavigationBar
{
    _btnBack.enabled = FALSE;
}

- (void)configureView
{
    _btnDone.enabled = FALSE;
    
    _lblTimeout.hidden = NO;
    _lblTimeout.text = [NSString stringWithFormat:@"%ld", (long)timeout];
}

#pragma mark - Buttons handlers

- (IBAction)btnBackPress:(id)sender
{
    [self registrationBack];
}

- (IBAction)btnDonePress:(id)sender
{
    if ([_tfSMSCode.text isEmpty])
    {
        [UIAlertController showSimpleAlertWithTitle:NSLocalizedString(@"error", nil)
                                            message:NSLocalizedString(@"sms_code_is_not_specified", nil)
                                   onViewController:self];
        
        return;
    }
    
    [self registrationNext];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    _btnDone.enabled = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if (resultString && ![resultString isEmpty])
    {
        if (resultString.length == 4) {
            _btnDone.enabled = YES;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    _btnDone.enabled = NO;
    
    return YES;
}

#pragma mark - Addition handlers

- (void)startTimer
{
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                             target:self
                                           selector:@selector(timerFired:)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)stopTimer
{
    if (timer && [timer isValid]) {
        [timer invalidate];
    }
    
    timer = nil;
}

- (void)timerFired:(NSTimer *)sender
{
    if (sender == timer)
    {
        if (timeout > 0) {
            timeout--;
        } else {
            _lblTimeout.hidden = YES;
             _btnBack.enabled = TRUE;
            [self stopTimer];
        }
        
        _lblTimeout.text = [NSString stringWithFormat:@"%ld", (long)timeout];
    }
}

#pragma mark - Keyboard handlers

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - Segues handlers

- (void)registrationBack
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)registrationNext
{
    [self performSegueWithIdentifier:@"REGISTRATION_SMS_CONFIRM_DONE_SEGUE" sender:_btnDone];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[YNRegistrationUserDetailsViewController class]])
    {
        YNRegistrationUserDetailsViewController *registrationUserDetailsViewController = (YNRegistrationUserDetailsViewController*)[segue destinationViewController];
        
        if (sender == _btnDone)
        {
            registrationUserDetailsViewController.userPhoneNumber = _userPhoneNumber;
            registrationUserDetailsViewController.userPassword = _userPassword;
            registrationUserDetailsViewController.smsCode = [[NSNumberFormatter new] numberFromString:_tfSMSCode.text];
        }
    }
}

@end
