//
//  YNRegistrationUserDetailsViewController.h
//  Yenot
//
//  Created by FanLee on 21.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNRegistrationUserDetailsViewController : UIViewController <UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) YNDTONumber *userPhoneNumber;

@property (nonatomic, strong) YNPassword *userPassword;

@property (nonatomic, strong) NSNumber *smsCode;


@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnBack;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnDone;

@property (nonatomic, weak) IBOutlet UIButton *btnGallery;

@property (nonatomic, weak) IBOutlet UIButton *btnTakePhoto;

@property (nonatomic, weak) IBOutlet UIImageView *userPhotoImageView;

@property (nonatomic, weak) IBOutlet UITextField *tfNickName;

- (IBAction)btnBackPress:(id)sender;

- (IBAction)btnDonePress:(id)sender;

- (IBAction)btnGalleryPress:(id)sender;

- (IBAction)btnTakePhotoPress:(id)sender;

@end
