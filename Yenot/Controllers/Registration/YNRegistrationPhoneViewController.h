//
//  YNRegistrationPhoneViewController.h
//  Yenot
//
//  Created by FanLee on 10.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNRegistrationPhoneViewController : UIViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnDone;

@property (nonatomic, weak) IBOutlet UIButton *btnCountry;

@property (nonatomic, weak) IBOutlet UILabel *lblCode;

@property (nonatomic, weak) IBOutlet UITextField *tfPhoneNumber;

@property (nonatomic, weak) IBOutlet UIPickerView *countryPickerView;

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *countryPickerViewConstraintBottom;

- (IBAction)btnDonePress:(id)sender;

- (IBAction)btnCountryPress:(id)sender;

@end
