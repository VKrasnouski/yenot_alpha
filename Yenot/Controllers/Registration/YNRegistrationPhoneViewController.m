//
//  YNRegistrationPhoneViewController.m
//  Yenot
//
//  Created by FanLee on 10.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNRegistrationPhoneViewController.h"
#import "YNRegistrationSMSViewController.h"
#import <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <CoreTelephony/CTCarrier.h>

@implementation YNRegistrationPhoneViewController
{
    NSDictionary *countriesDiallingCodes;
    NSMutableDictionary *countriesCodes;
    NSMutableArray *countriesNames;
    YNDTONumber *userPhoneNumber;
    YNPassword *userPassword;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super initWithCoder:aDecoder])
    {
        // Perform all initializations here
        userPhoneNumber = nil;
        userPassword = nil;
        countriesDiallingCodes = [[NSDictionary alloc] init];
        countriesCodes = [[NSMutableDictionary alloc] init];
        countriesNames = [[NSMutableArray alloc] init];
        
        NSString *diallingCodesPlistPath = [[NSBundle mainBundle] pathForResource:@"RPDiallingCodes" ofType:@"plist"];
        countriesDiallingCodes = [NSMutableDictionary dictionaryWithContentsOfFile:diallingCodesPlistPath];
        
        for (NSString *countryCode in countriesDiallingCodes)
        {
            NSString *countryName = [self getCountryNameByCountryCode:countryCode];
            
            if (countryName)
            {
                [countriesNames addObject:countryName];
                [countriesCodes setValue:countryCode forKey:countryName];
            }
        }
        
        [countriesNames sortUsingComparator:^NSComparisonResult(id  _Nonnull obj1, id  _Nonnull obj2) {
            
            return [(NSString *)obj1 localizedCompare:(NSString *)obj2];
            
        }];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
}

#pragma mark - Configure handlers

- (void)configureView
{
    _btnDone.enabled = NO;
    
    NSString *currentCountryCode = [self getCurrentCountryCode];
    NSString *currentCountryName = [self getCountryNameByCountryCode:currentCountryCode];
    NSString *currentDiallingCode = [self getDiallingCodeByCountryCode:currentCountryCode];
    
    [_btnCountry setTitle:currentCountryName forState:UIControlStateNormal];
    _lblCode.text = [NSString stringWithFormat:@"+%@", currentDiallingCode];
    
    _countryPickerViewConstraintBottom.constant = _countryPickerView.frame.size.height * -1;
}

#pragma mark - Buttons handlers

- (IBAction)btnDonePress:(id)sender
{
    _btnDone.enabled = NO;
    
    if ([_tfPhoneNumber.text isEmpty])
    {
        [UIAlertController showSimpleAlertWithTitle:NSLocalizedString(@"error", nil)
                                            message:NSLocalizedString(@"phone_number_is_not_specified", nil)
                                   onViewController:self];
        
        return;
    }
    
    if (![_tfPhoneNumber.text convertToPhoneNumber])
    {
        [UIAlertController showSimpleAlertWithTitle:NSLocalizedString(@"error", nil)
                                            message:NSLocalizedString(@"phone_number_is_incorrect", nil)
                                   onViewController:self];
        
        return;
    }
    
    NSMutableString *fullPhoneNumber = [[NSMutableString alloc] initWithString:_lblCode.text];
    [fullPhoneNumber appendString:_tfPhoneNumber.text];

    YNDTONumber *number = [[YNDTONumber alloc] init];
    number.number = [fullPhoneNumber convertToPhoneNumber];
    
    [YNRequestManager requestPOSTRegistrationStartSMSWithPhoneNumber:number
                                                             success:^(YNPassword *password)
    {
        userPhoneNumber = number;
        userPassword = password;

        _btnDone.enabled = YES;

        [self registrationNext];
        
     } failure:^(NSError *error) {
         
         _btnDone.enabled = YES;
         
     }];
}

- (IBAction)btnCountryPress:(id)sender
{
    [self.view endEditing:YES];
    [self showCountryPickerView];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    _btnDone.enabled = NO;
    
    NSString *resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];

    if (resultString && ![resultString isEmpty])
    {
        // Сейчас ограничение только на белорусский номер
        if (resultString.length == 9) {
            _btnDone.enabled = YES;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    _btnDone.enabled = NO;
    
    return YES;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return countriesNames.count;
}

#pragma mark - UIPickerViewDataSource

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *countryName = countriesNames[row];
    
    return [NSString stringWithFormat:@"%@ (%@)", countryName, countriesDiallingCodes[countriesCodes[countryName]]];
}

#pragma mark - UIPickerViewDelegate

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSString *countryName = countriesNames[row];
    
    _lblCode.text = countriesDiallingCodes[countriesCodes[countryName]];
    [_btnCountry setTitle:countryName forState:UIControlStateNormal];
}

#pragma mark - Keyboard handlers

- (void)touchesBegan:(NSSet <UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    [self hideCountryPickerView];
}

#pragma mark - Additional handlers

- (void)showCountryPickerView
{
    _countryPickerViewConstraintBottom.constant = 0;
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (void)hideCountryPickerView
{
    _countryPickerViewConstraintBottom.constant = _countryPickerView.frame.size.height * -1;
    
    [UIView animateWithDuration:0.25 animations:^{
        [self.view layoutIfNeeded];
    }];
}

- (NSString *)getCurrentCountryCode
{
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    return [[carrier isoCountryCode] uppercaseString];
}

- (NSString *)getCountryNameByCountryCode:(NSString *)countryCode
{
    NSLocale *locale = [NSLocale currentLocale];
    NSString *countryName = [locale displayNameForKey:NSLocaleCountryCode
                                                value:[countryCode uppercaseString]];
    
    return countryName;
}

- (NSString *)getDiallingCodeByCountryCode:(NSString *)countryCode
{
    NSString *diallingCodesPlistPath = [[NSBundle mainBundle] pathForResource:@"RPDiallingCodes" ofType:@"plist"];
    NSDictionary *diallingCodesDictionary = [NSDictionary dictionaryWithContentsOfFile:diallingCodesPlistPath];
    
    return diallingCodesDictionary[[countryCode lowercaseString]];
}

#pragma mark - Segues handlers

- (void)registrationNext
{
    [self performSegueWithIdentifier:@"REGISTRATION_NUMBER_DONE_SEGUE" sender:_btnDone];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue destinationViewController] isKindOfClass:[YNRegistrationSMSViewController class]])
    {
        YNRegistrationSMSViewController *registrationSMSViewController = (YNRegistrationSMSViewController*)[segue destinationViewController];
        
        if (sender == _btnDone)
        {
            registrationSMSViewController.userPhoneNumber = userPhoneNumber;
            registrationSMSViewController.userPassword = userPassword;
        }
    }
}

@end
