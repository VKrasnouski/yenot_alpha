//
//  YNRegistrationSMSViewController.h
//  Yenot
//
//  Created by FanLee on 10.02.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNRegistrationSMSViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) YNDTONumber *userPhoneNumber;

@property (nonatomic, strong) YNPassword *userPassword;


@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnBack;

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnDone;

@property (nonatomic, weak) IBOutlet UITextField *tfSMSCode;

@property (nonatomic, weak) IBOutlet UILabel *lblTimeout;

- (IBAction)btnBackPress:(id)sender;

- (IBAction)btnDonePress:(id)sender;

@end
