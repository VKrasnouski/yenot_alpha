//
//  YNRootViewController.h
//  Yenot
//
//  Created by FanLee on 11.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YNRootViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end
