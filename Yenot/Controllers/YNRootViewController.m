//
//  YNRootViewController.m
//  Yenot
//
//  Created by FanLee on 11.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNRootViewController.h"

@implementation YNRootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
}

#pragma mark - Configuration methods

- (void)configureView
{
    [_activityIndicatorView startAnimating];
}

@end
