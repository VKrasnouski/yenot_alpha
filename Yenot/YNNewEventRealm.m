//
//  YNNewEventRealm.m
//  Yenot
//
//  Created by FanLee on 22/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import "YNNewEventRealm.h"

@implementation YNNewEventRealm

-(id)initWithObjects: (YNEventRealm *)event eventDate:(YNEventDateRealm *)eventDate{
    self = [super init];
    if(!self) return nil;
    _event = event;
    _eventDate = eventDate;
     return self;
}


@end
