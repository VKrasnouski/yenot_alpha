//
//  YNUseCaseManager.m
//  Yenot
//
//  Created by FanLee on 11.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import "YNUseCaseManager.h"
#import "AppDelegate.h"

@implementation YNUseCaseManager

+ (void)hideAllViewControllers:(BOOL)animated
{
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.window.rootViewController dismissViewControllerAnimated:animated completion:nil];
}

+ (void)showInitialViewControllerFromStoryboard:(nonnull NSString*)storyboardName animated:(BOOL)animated
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    UIViewController *initialViewController = [mainStoryboard instantiateInitialViewController];
    
    [YNUseCaseManager hideAllViewControllers:animated];
    
    AppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate.window.rootViewController presentViewController:initialViewController animated:animated completion:nil];
}

+ (void)startRegistrationUseCase
{
    [YNUseCaseManager showInitialViewControllerFromStoryboard:@"YNRegistrationStoryboard" animated:NO];
}

+ (void)startMainUseCase
{
    [YNUseCaseManager showInitialViewControllerFromStoryboard:@"YNMainStoryboard" animated:NO];
}

@end
