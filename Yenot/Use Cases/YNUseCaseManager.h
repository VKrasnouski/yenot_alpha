//
//  YNUseCaseManager.h
//  Yenot
//
//  Created by FanLee on 11.03.16.
//  Copyright © 2016 Vasili Krasnouski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YNUseCaseManager : NSObject

+ (void)hideAllViewControllers:(BOOL)animated;

+ (void)showInitialViewControllerFromStoryboard:(nonnull NSString*)storyboardName animated:(BOOL)animated;

+ (void)startRegistrationUseCase;

+ (void)startMainUseCase;

@end
