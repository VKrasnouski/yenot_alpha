//
//  YNEventDateRealm.m
//  Yenot
//
//  Created by FanLee on 22/01/2017.
//  Copyright © 2017 Vasili Krasnouski. All rights reserved.
//

#import "YNEventDateRealm.h"

@implementation YNEventDateRealm

- (id)initWithMantleModel:(YNEventDate *)yenotEventDate{
    self = [super init];
    if(!self) return nil;
    
    _ID = yenotEventDate.ID;
    _version = yenotEventDate.version;
    _period = yenotEventDate.period;
    _periodInterval = yenotEventDate.periodInterval;
    _cancelled = yenotEventDate.cancelled;
    _eventID = yenotEventDate.eventId;
    _subEventID = yenotEventDate.subeventId;
    //_days =  [NSKeyedArchiver archivedDataWithRootObject:yenotEventDate.days];
    _dateTimeEnd = yenotEventDate.datetimeEnd;
    _dateTimeStart = yenotEventDate.datetimeStart;
    _timeEnd = yenotEventDate.timeEnd;
    _timeStart = yenotEventDate.timeStart;
    
    
    return self;

}



@end
